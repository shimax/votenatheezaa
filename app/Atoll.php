<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atoll extends Model
{
  public function islands(){
    return $this->hasMany(Island::class, 'atoll_reg_no','reg_no');
  }
    //
}
