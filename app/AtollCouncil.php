<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtollCouncil extends Model
{

  public function atoll(){
    return $this->belongsTo(Atoll::class, 'atoll_id');
  }
  public function sections(){
    return $this->hasMany(AtolLCouncilSection::class, 'atoll_council_id');
  }
    //
}
