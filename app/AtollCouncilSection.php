<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtollCouncilSection extends Model
{

  public function atoll_council(){
    return $this->belongsTo(AtollCouncil::class, 'atoll_council_id');
  }

  public function  ballot_boxes(){
    return $this->hasMany(AtollCouncilSectionBallotBox::class, 'atoll_council_section_id');
  }
    //
}
