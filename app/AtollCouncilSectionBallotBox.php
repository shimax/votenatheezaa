<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtollCouncilSectionBallotBox extends Model
{

  public function atoll_council_section(){
    return $this->belongsTo(AtollCouncilSection::class, 'atoll_council_section_id');
  }

  public function details(){
    return $this->hasMany(AtollCouncilSectionBallotBoxDetail::class, 'atoll_council_section_ballot_box_id');
  }
    //
}
