<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtollCouncilSectionBallotBoxDetail extends Model
{

  public function ballot_box(){
    return $this->belongsTo(AtollCouncilSectionBallotBox::class, 'atoll_council_section_ballot_box_id');
  }
    //
}
