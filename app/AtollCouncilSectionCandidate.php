<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtollCouncilSectionCandidate extends Model
{

  public function candidate(){
    return $this->belongsTo(Candidate::class, 'candidate_id');
  }

  public function atoll_council_section(){
    return $this->belongsTo(AtollCouncilSection::class, 'atoll_council_section_id');
  }
    //
}
