<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BallotBox extends Model
{

  public function island(){
    return $this->belongsTo(Island::class, 'island_id');
  }
  public function atoll(){
    return $this->belongsTo(Atoll::class, 'atoll_id');
  }

  public function atoll_council_section(){
    return $this->belongsTo(AtollCouncilSection::class,  'atoll_council_section_id');
  }

  public function details(){
    return $this->hasMany(BallotBoxDetail::class, 'ballot_box_id');
  }
    //
}
