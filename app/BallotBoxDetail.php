<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BallotBoxDetail extends Model
{

  public function candidate(){
    return $this->belongsTo(Candidate::class,'candidate_id');
  }

  public function dhaairaa(){
    return $this->belongsTo(AtollCouncilSection::class, 'atoll_council_section_id');
  }
    //
}
