<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{

  public function political_party(){
    return $this->belongsTo(PoliticalParty::class, 'political_party_id');
  }

  public function atoll_council_section_candidate(){
    return $this->belongsTo(AtollCouncilSectionCandidate::class,'candidate_id' );
  }

  public function island_council_candidate(){
    return $this->belongsTo(IslandCouncilCandidate::class,'candidate_id' );
  }
    //
}
