<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityCouncil extends Model
{
    public function atoll(){
        return $this->belongsTo(Atoll::class, 'atoll_id');
    }
    public function sections(){
        return $this->hasMany(CityCouncilSection::class, 'city_council_id');
    }
}
