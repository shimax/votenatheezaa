<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityCouncilSection extends Model
{
    public function city_council() {
        return $this->belongsTo(CityCouncil::class, 'city_council_id');
    }

}
