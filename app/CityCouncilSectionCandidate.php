<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityCouncilSectionCandidate extends Model
{
    
    public function candidate(){
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }

    public function city_council_section(){
        return $this->belongsTo(CityCouncilSection::class, 'city_council_section_id');
    }
}
