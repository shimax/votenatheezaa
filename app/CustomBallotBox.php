<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomBallotBox extends Model
{

    public function details(){
        return $this->hasMany(CustomBallotBoxDetail::class, 'custom_ballot_box_id');
    }
    //
}
