<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atoll;

use App\Candidate;
use App\AtollCouncilSection;
use App\AtollCouncilSectionCandidate;
use App\PoliticalParty;

class AtollCouncilCandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $political_parties = PoliticalParty::get();

      $atoll_council_candidates = AtollCouncilSectionCandidate::with('candidate', 'atoll_council_section.atoll_council.atoll')->get();
      $atoll_council_sections = AtollCouncilSection::with('atoll_council.atoll')->get();



      $atoll_with_council_sections_array = [];

      foreach($atoll_council_sections as $atoll_council_section){
        $atoll_with_council_section = new \StdClass();
        $atoll_with_council_section->name = $atoll_council_section->atoll_council->atoll->english_atoll_name_short.' '.$atoll_council_section->name;
        $atoll_with_council_section->atoll_council_section_id = $atoll_council_section->id;
        $atoll_with_council_sections_array[] = $atoll_with_council_section;
      }

      $atoll_with_council_sections = collect($atoll_with_council_sections_array);

      return view('dashboard.atoll_council_section_candidate.index',[
        'political_parties' => $political_parties,
        'atoll_council_candidates' => $atoll_council_candidates,
        'atoll_council_sections' => $atoll_with_council_sections

       ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $candidate = new Candidate;
      $candidate->name = $request->name;
      $candidate->political_party_id = $request->political_party_id;
      $candidate->number = $request->number;
      $candidate->save();

      $atoll_council_section_candidate = new AtollCouncilSectionCandidate;
      $atoll_council_section_candidate->candidate_id = $candidate->id;
      $atoll_council_section_candidate->atoll_council_section_id = $request->atoll_council_section_id;
      $atoll_council_section_candidate->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $atoll_council_candidate = AtollCouncilSectionCandidate::find($id);

      $candidate = Candidate::find($atoll_council_candidate->candidate_id);
      $candidate->delete();

      $atoll_council_candidate->delete();

      return redirect()->back();




        //
    }
}
