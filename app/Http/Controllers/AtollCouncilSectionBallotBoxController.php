<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AtollCouncil;
use App\AtollCouncilSection;
use App\AtollSectionBallotBox;

class AtollCouncilSectionBallotBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $atoll_council_sections = AtollCouncilection::with('atoll_council.atoll')->get();
      $atoll_council_section_ballot_boxes =  AtollCouncilSectionBallotBox::with('atoll_council_section')->get();
      return view('dashboard.atoll_council.index', compact('atoll_council_section_ballot_boxes','atoll_council_sections'));


        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $atoll_council_section_ballot_box = new AtollCouncilSectionBallotBox;
      $atoll_council_section_ballot_box->atoll_council_section_id = $request->atoll_council_section_id;
      $atoll_council_section_ballot_box->name  =$request->name;
      $atoll_council_section_ballot_box->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
