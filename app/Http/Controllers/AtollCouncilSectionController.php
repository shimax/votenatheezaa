<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atoll;
use App\AtollCouncilSection;


class AtollCouncilSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $atolls = Atoll::get();
      $atoll_council_sections = AtollCouncilSection::with('atoll_council.atoll')->get();



      return view('dashboard.atoll_council_section.index', ['atolls' => $atolls, 'atoll_council_sections' => $atoll_council_sections]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $atoll_council_section = new AtollCouncilSection;
      $atoll_council_section->name = $request->name;
      $atoll_council_section->atoll_council_id = $request->atoll_council_id;
      $atoll_council_section->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $atoll_council_section = AtollCouncilSection::find($id);
      $atoll_council_section->delete();

      return redirect()->back();
        //
    }
}
