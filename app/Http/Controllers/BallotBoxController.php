<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Island;
use App\BallotBox;
use App\BallotBoxDetail;
use App\AtollCouncilSectionCandidate;
use App\IslandCouncilCandidate;
use App\AtollCouncilSection;


class BallotBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $islands = Island::with('atoll')->get();

      $islands_with_atoll_array = [];
      foreach($islands as $island){
        $island_with_atoll = new \StdClass();
        $island_with_atoll->name =  $island->atoll->english_atoll_name_long.' '.$island->english_official_island_name;
        $island_with_atoll->id = $island->id;
        $islands_with_atoll_array[] =$island_with_atoll;
      }

      $islands_with_atoll = collect($islands_with_atoll_array);

      $ballot_boxes = BallotBox::with('island','atoll','atoll_council_section')->get();

      $atoll_council_sections = AtollCouncilSection::get();



      return view('dashboard.ballot_box.index',[
        'ballot_boxes' => $ballot_boxes,
       'formatted_islands' => $islands_with_atoll,
       'atoll_council_sections' => $atoll_council_sections

     ]);

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $island_with_atoll = Island::with('atoll')->find($request->island_id);


      $ballot_box = new BallotBox;
      $ballot_box->number = $request->number;
      $ballot_box->name = $request->name;
      $ballot_box->location = $request->location;
      $ballot_box->island_id = $request->island_id;
      $ballot_box->atoll_id = $island_with_atoll->atoll->id;
      $ballot_box->atoll_council_section_id = $request->atoll_council_section_id;
      $ballot_box->total_votes = 0;
      $ballot_box->valid_votes = 0;
      $ballot_box->invalid_votes = 0;
      $ballot_box->save();

      return redirect()->back();



        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $ballot_box = BallotBox::with('island','atoll')->find($id);
      $atoll_council_section_id = $ballot_box->atoll_council_section_id;
      $island_id = $ballot_box->island->id;


      $atoll_council_section_candidates = AtollCouncilSectionCandidate::with('candidate.political_party')->where('atoll_council_section_id',  $atoll_council_section_id)->get();

      $island_council_section_candidates = IslandCouncilCandidate::with('candidate.political_party')->whereHas('island', function($query) use ($island_id){
        $query->where('island_id',$island_id);
      })->get();


      if($island_council_section_candidates!= null){
        foreach($island_council_section_candidates as $island_council_section_candidate){
          $ballotbox_detail = BallotBoxDetail::where('candidate_id', $island_council_section_candidate->candidate_id)->where('ballot_box_id', $ballot_box->id)->get();

          if($ballotbox_detail == '[]'){
            $ballotbox_detail = new BallotBoxDetail;
            $ballotbox_detail->candidate_id = $island_council_section_candidate->candidate_id;
            $ballotbox_detail->ballot_box_id = $ballot_box->id;
            $ballotbox_detail->council_type_id = '2';
            $ballotbox_detail->save();
          }
        }
      }


      if($atoll_council_section_candidates!=null){
        foreach($atoll_council_section_candidates as $atoll_council_section_candidate){
          $ballotbox_detail = BallotBoxDetail::where('candidate_id', $atoll_council_section_candidate->candidate_id)->where('ballot_box_id', $ballot_box->id)->get();

          if($ballotbox_detail == '[]'){
            $ballotbox_detail = new BallotBoxDetail;
            $ballotbox_detail->candidate_id = $atoll_council_section_candidate->candidate_id;
            $ballotbox_detail->ballot_box_id = $ballot_box->id;
            $ballotbox_detail->atoll_council_section_id = $atoll_council_section_candidate->atoll_council_section_id;
            $ballotbox_detail->council_type_id = '1';
            $ballotbox_detail->save();

          }
        }
      }

      $ballot_box->load('details.dhaairaa');
      $ballot_box->load('details.candidate.political_party');





      return view('dashboard.ballot_box.show',['ballot_box' => $ballot_box]);



        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $ballot_box = BallotBox::find($id);
      $ballot_box->valid_votes = $request->valid_votes;
      $ballot_box->invalid_votes = $request->invalid_votes;
      $ballot_box->total_votes = $request->valid_votes + $request->invalid_votes;
      $ballot_box->update();

      return redirect()->back();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $ballot_box = BallotBox::find($id);
      $ballot_box->delete();

      return redirect()->back();

        //
    }
}
