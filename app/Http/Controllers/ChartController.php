<?php

namespace App\Http\Controllers;

use App\AtollCouncilSection;
use App\BallotBox;
use App\CityCouncilSection;
use App\CustomBallotBox;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\BallotBoxDetail;
use App\CustomBallotBoxDetail;

class ChartController extends Controller
{

  public function display_ballot_box_of_island_council($bbid)
  {
    $tesBallox = BallotBox::find($bbid);

    $ballot_box_details = BallotBoxDetail::select('candidate_id','votes')->where('atoll_council_section_id', NULL)->where('ballot_box_id', $bbid)->distinct()->get();

    $ballot_box_details->load('candidate.political_party');



    $test = '';
    $color = "silver";

    $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';
    //  $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';


    foreach ($ballot_box_details as $key => $ballot_box_detail) {
      if($ballot_box_detail->candidate->political_party->name == "PPM"){
        $default_color = 'fill-color:  #ff52b8';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDP"){
        $default_color = 'fill-color: yellow';
      }elseif($ballot_box_detail->candidate->political_party->name == "JP"){
        $default_color = 'fill-color: red';
      }elseif($ballot_box_detail->candidate->political_party->name == "Adhaalath"){
        $default_color = 'fill-color: green';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDA"){
        $default_color = 'fill-color: orange';
      }elseif($ballot_box_detail->candidate->political_party->name == "DRP"){
        $default_color = 'fill-color: blue';
      }
      else{
        $default_color = 'fill-color: silver';
      }

      $votes = (int)$ballot_box_detail->votes;

      $test .= json_encode(array($ballot_box_detail->candidate->name, $votes, $default_color, $votes));
      $test .= ",";
      //
      //
      // $chart_display = new \StdClass();
      // $chart_display->name = $ballot_box_detail->candidate->name;
      // $chart_display->votes = $ballot_box_detail->votes;
      // $chart_array[] = $test;
    }

//    dd($test);
    return view('charts.atoll_council', ['string' => $test, 'tesBallox' => $tesBallox]);



  }



  public function display_ballot_box_of_atoll_council($bbid)
  {
    $tesBallox = BallotBox::find($bbid);
    $ballot_box_details = BallotBoxDetail::select('candidate_id','votes')->where('atoll_council_section_id', '!=', 'NULL')->where('ballot_box_id',$bbid)->distinct()->get();

    $ballot_box_details->load('candidate.political_party');


    $test = '';
    $color = "silver";

    $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';
    //  $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';


    foreach ($ballot_box_details as $key => $ballot_box_detail) {
      if($ballot_box_detail->candidate->political_party->name == "PPM"){
        $default_color = 'fill-color:  #ff52b8';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDP"){
        $default_color = 'fill-color: yellow';
      }elseif($ballot_box_detail->candidate->political_party->name == "JP"){
        $default_color = 'fill-color: red';
      }elseif($ballot_box_detail->candidate->political_party->name == "Adhaalath"){
        $default_color = 'fill-color: green';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDA"){
        $default_color = 'fill-color: orange';
      }elseif($ballot_box_detail->candidate->political_party->name == "DRP"){
        $default_color = 'fill-color: blue';
      }
      else{
        $default_color = 'fill-color: silver';
      }


      $votes = (int)$ballot_box_detail->votes;
      $test .= json_encode(array($ballot_box_detail->candidate->name, $votes, $default_color, $votes));
      $test .= ",";
      //
      // $string2 = '  ["MDP Candidate", 8, " fill-color: yellow",8],
      //   ["Independent Candidate B", 10, "silver", 10],
      //   ["PPM Candidate", 23, " fill-color:  #ff52b8", 23],
      //   ["MDP Candidate D", 21, " fill-color: yellow",21],';
      //


      //
      //
      // $chart_display = new \StdClass();
      // $chart_display->name = $ballot_box_detail->candidate->name;
      // $chart_display->votes = $ballot_box_detail->votes;
      // $chart_array[] = $test;
    }
    return view('charts.atoll_council', ['string' => $test, 'tesBallox' => $tesBallox]);



  }

  public function display_custom_ballot_box($bbid){
    $tesBallox = CustomBallotBox::find($bbid);

    $ballot_box_details = CustomBallotBoxDetail::with('candidate.political_party')->where('custom_ballot_box_id', $bbid)->get();



    $test = '';
    $color = "silver";

    $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';
    //  $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';


    foreach($ballot_box_details as $key => $ballot_box_detail){
      if($ballot_box_detail->candidate->political_party->name == "PPM"){
        $default_color = 'fill-color:  #ff52b8';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDP"){
        $default_color = 'fill-color: yellow';
      }elseif($ballot_box_detail->candidate->political_party->name == "JP"){
        $default_color = 'fill-color: red';
      }elseif($ballot_box_detail->candidate->political_party->name == "Adhaalath"){
        $default_color = 'fill-color: green';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDA"){
        $default_color = 'fill-color: orange';
      }elseif($ballot_box_detail->candidate->political_party->name == "DRP"){
        $default_color = 'fill-color: blue';
      }
      else{
        $default_color = 'fill-color: silver';
      }

      $votes = (int)$ballot_box_detail->votes;

      $test .= json_encode(array($ballot_box_detail->candidate->name, $votes ,$default_color,$votes));
      $test .=",";
      //
      //
      // $chart_display = new \StdClass();
      // $chart_display->name = $ballot_box_detail->candidate->name;
      // $chart_display->votes = $ballot_box_detail->votes;
      // $chart_array[] = $test;
    }


    // $chart_object = collect($chart_array);
    //
    // $string = json_encode($chart_array);

    // $string2 = '  ["MDP Candidate", 8, " fill-color: yellow",8],
    //   ["Independent Candidate B", 10, "silver", 10],
    //   ["PPM Candidate", 23, " fill-color:  #ff52b8", 23],
    //   ["MDP Candidate D", 21, " fill-color: yellow",21],';

    // $string2 = '  ["MDP Candidate", 8, " fill-color: yellow",8],
    //   ["Independent Candidate B", 10, "silver", 10],
    //   ["PPM Candidate", 23, " fill-color:  #ff52b8", 23],
    //   ["MDP Candidate D", 21, " fill-color: yellow",21],';

// dd($test,$string2);


    return view('charts.atoll_council', ['string' => $test, 'tesBallox' => $tesBallox]);
  }





  public function display_ballot_box_of_city_council($bbid){
    $tesBallox = BallotBox::find($bbid);

    $allCollection = CityCouncilSection::pluck('name','id')->merge(AtollCouncilSection::pluck('name','id'));

    $ballot_box_details = BallotBoxDetail::select('candidate_id')->where('city_council_section_id','!=','NULL')->distinct()->get();

    $ballot_box_details->load('candidate.political_party');

    $test = '';
    $color = "silver";

    $default_color = 'stroke-color: grey; stroke-width: 4; fill-color: silver';


    foreach($ballot_box_details as $key => $ballot_box_detail){
      if($ballot_box_detail->candidate->political_party->name == "PPM"){
        $default_color = 'fill-color:  #ff52b8';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDP"){
        $default_color = 'fill-color: yellow';
      }elseif($ballot_box_detail->candidate->political_party->name == "JP"){
        $default_color = 'fill-color: red';
      }elseif($ballot_box_detail->candidate->political_party->name == "Adhaalath"){
        $default_color = 'fill-color: green';
      }elseif($ballot_box_detail->candidate->political_party->name == "MDA"){
        $default_color = 'fill-color: orange';
      }elseif($ballot_box_detail->candidate->political_party->name == "DRP"){
        $default_color = 'fill-color: blue';
      }
      else{
        $default_color = 'fill-color: silver';
      }


      $votes = (int)$ballot_box_detail->votes;

      $test .= json_encode(array($ballot_box_detail->candidate->name, $votes ,$default_color,$votes));
      $test .=",";
      //
      //
      // $chart_display = new \StdClass();
      // $chart_display->name = $ballot_box_detail->candidate->name;
      // $chart_display->votes = $ballot_box_detail->votes;
      // $chart_array[] = $test;
    }


    // $chart_object = collect($chart_array);
    //
    // $string = json_encode($chart_array);

//    $string2 = '  ["MDP Candidate", 8, "stroke-color: #ffb300; stroke-width: 4; fill-color: yellow",8],
//      ["Independent Candidate B", 10, "silver", 10],
//      ["PPM Candidate", 23, "stroke-color: #ec008c; stroke-width: 4; fill-color:  #ff52b8", 23],
//      ["MDP Candidate D", 21, "stroke-color: #ffb300; stroke-width: 4; fill-color: yellow",21],';

// dd($test,$string2);


    return view('charts.atoll_council', ['string' => $test, 'tesBallox' => $tesBallox]);
  }
}
