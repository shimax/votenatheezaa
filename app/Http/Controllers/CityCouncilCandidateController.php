<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\CityCouncil;
use App\CityCouncilSection;
use App\CityCouncilSectionCandidate;
use App\PoliticalParty;
use Illuminate\Http\Request;

class CityCouncilCandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $political_parties = PoliticalParty::get();

        $city_council_sections = CityCouncilSection::with('city_council')->get();

        $city_council_candidates = CityCouncilSectionCandidate::with('candidate', 'city_council_section.city_council.atoll')->get();


        
        return view('dashboard.city_council_candidate.index',[
            'city_council_sections' => $city_council_sections,
            'political_parties' => $political_parties,
            'city_council_candidates' => $city_council_candidates
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $candidate = new Candidate();
        $candidate->name = $request->name;
        $candidate->political_party_id = $request->political_party_id;
        $candidate->number = $request->number;
        $candidate->save();

        $city_council_section_candidate = new CityCouncilSectionCandidate();
        $city_council_section_candidate->candidate_id = $candidate->id;
        $city_council_section_candidate->city_council_section_id = $request->city_council_section_id;
        $city_council_section_candidate->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city_council_section_candidate = CityCouncilSectionCandidate::find($id);

        $candidate = Candidate::find($city_council_section_candidate->candidate_id);
        $candidate->delete();

        $city_council_section_candidate->delete();

        return redirect()->back();
    }
}
