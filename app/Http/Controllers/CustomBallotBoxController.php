<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomBallotBox;
use App\CustomBallotBoxDetail;

use App\Candidate;
use App\AtollCouncilSectionCandidate;
use App\IslandCouncilCandidate;

class CustomBallotBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custom_ballot_boxes = CustomBallotBox::get();
        return view('dashboard.custom_ballot_box.index',[
            'custom_ballot_boxes' => $custom_ballot_boxes
        ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ballot_box = new CustomBallotBox;
        $ballot_box->number = $request->number;
        $ballot_box->name = $request->name;
        $ballot_box->location = $request->location;
        $ballot_box->total_votes = 0;
        $ballot_box->valid_votes = 0;
        $ballot_box->invalid_votes = 0;
        $ballot_box->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $custom_ballot_box = CustomBallotBox::with('details')->find($id);
        $all_candidates = Candidate::get();

        $candidate_array = [];




        foreach($all_candidates as $candidate){
            $new_candidate = new \StdClass();

            $candidate->load('atoll_council_section_candidate.atoll_council_section');

            $atoll = AtollCouncilSectionCandidate::with('atoll_council_section')->where('candidate_id', $candidate->id)->first();
            $island = IslandCouncilCandidate::with('island','atoll')->where('candidate_id', $candidate->id)->first();

            if($atoll!= null){
              $new_candidate->name = 'ATOLL COUNCIL '.$atoll->atoll_council_section->name.' '.$candidate->name;
            }else if($island!= null){
              $new_candidate->name = ' ISLAND COUNCIL '.$island->atoll->english_atoll_name_long.' '.$island->island->english_official_island_name.' '.$candidate->name;
            }else{
              $new_candidate->name = ' CITY COUNCIL '.$candidate->name;

            }


            $candidate->load('island_council_candidate.island');

            // if($candidate->atoll_council_section_candidate != null){
            //     $new_candidate->name = $candidate->atoll_council_section_candidate->atoll_council_section->name.' '.$candidate->name;
            // }
            //
            // if($candidate->island_council_candidate != null){
            //     $new_candidate->name = $candidate->island_council_candidate->island->english_official_island_name.' '.$candidate->name;
            //
            // }
            // $new_candidate->name = ''.$candidate->name;
            $new_candidate->id = $candidate->id;


            $candidate_array[] =$new_candidate;
        }

        $formatted_candidates = collect($candidate_array);


        return view('dashboard.custom_ballot_box.show',['all_candidates' => $formatted_candidates, 'custom_ballot_box' => $custom_ballot_box]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function add_candidate(Request $request){

        $custom_ballot_box_detail = new CustomBallotBoxDetail;
        $custom_ballot_box_detail->custom_ballot_box_id = $request->custom_ballot_box_id;
        $custom_ballot_box_detail->candidate_id = $request->candidate_id;
        $custom_ballot_box_detail->save();

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ballot_box = CustomBallotBox::find($id);
        $ballot_box->valid_votes = $request->valid_votes;
        $ballot_box->invalid_votes = $request->invalid_votes;
        $ballot_box->total_votes = $request->valid_votes + $request->invalid_votes;
        $ballot_box->update();

        return redirect()->back();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ballot_box = CustomBallotBox::find($id);

        $custom_ballot_box_detail = CustomBallotBoxDetail::where('custom_ballot_box_id', $id)->first();
        if($custom_ballot_box_detail!= null) {

            $custom_ballot_box_detail->delete();
        }
        $ballot_box->delete();
        return redirect()->back();

        //
    }
}
