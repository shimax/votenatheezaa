<?php

namespace App\Http\Controllers;

use App\Atoll;
use App\Candidate;
use App\Island;
use App\IslandCouncilCandidate;
use App\PoliticalParty;
use Illuminate\Http\Request;

class IslandCouncilCandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $political_parties = PoliticalParty::get();

        $island_council_candidates = IslandCouncilCandidate::with('candidate.political_party', 'atoll', 'island')->get();

        $islands = Island::with('atoll')->get();

        $islands_with_atoll_array = [];
        foreach($islands as $island){
            $island_with_atoll = new \StdClass();
            $island_with_atoll->name =  $island->atoll->english_atoll_name_long.' '.$island->english_official_island_name;
            $island_with_atoll->id = $island->id;
            $islands_with_atoll_array[] =$island_with_atoll;
        }

        $islands_with_atoll = collect($islands_with_atoll_array);
        return view('dashboard.island_council_candidates.index',[
            'island_council_candidates' => $island_council_candidates,
            'political_parties' => $political_parties,
            'islands' => $islands_with_atoll
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $island = Island::with('atoll')->where('id', $request->island_id)->first();

        $candidate = new Candidate();
        $candidate->name = $request->name;
        $candidate->political_party_id = $request->political_party_id;
        $candidate->number = $request->number;
        $candidate->save();

        $island_council_candidate = new IslandCouncilCandidate();
        $island_council_candidate->atoll_id = $island->atoll->id;
        $island_council_candidate->island_id = $island->id;
        $island_council_candidate->candidate_id = $candidate->id;
        $island_council_candidate->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $island_council_candidate = IslandCouncilCandidate::find($id);

        $candidate = Candidate::find($island_council_candidate->candidate_id);
        $candidate->delete();

        $island_council_candidate->delete();

        return redirect()->back();

    }
}
