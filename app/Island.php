<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Island extends Model
{

  public function atoll(){
    return $this->belongsTo(Atoll::class, 'atoll_reg_no','reg_no');
  }
    //
}
