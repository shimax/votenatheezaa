<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IslandCouncilCandidate extends Model
{
    protected $fillable = ['*'];

    public function candidate(){
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }

    public function island(){
        return $this->belongsTo(Island::class, 'island_id');
    }

    public function atoll(){
        return $this->belongsTo(Atoll::class, 'atoll_id');
    }
}
