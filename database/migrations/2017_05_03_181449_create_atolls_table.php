<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('reg_no');
            $table->string('english_atoll_name_long');
            $table->string('english_atoll_name_short');
            $table->string('english_official_atoll_name');
            $table->string('dhivehi_atoll_name_long');
            $table->string('dhivehi_atoll_name_short');
            $table->string('dhivehi_official_atoll_name');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atolls');
    }
}
