<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtollCouncilSectionBallotBoxDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atoll_council_section_ballot_box_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('atoll_section_ballot_box_id');
            $table->integer('candidate_id');
            $table->integer('total_votes');
            $table->integer('valid_votes');
            $table->integer('invalid_votes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atoll_council_section_ballot_box_details');
    }
}
