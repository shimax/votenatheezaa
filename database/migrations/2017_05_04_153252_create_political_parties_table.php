<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticalPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('political_parties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $party = new \App\PoliticalParty();
        $party->name = 'MDP';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'PPM';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'DRP';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'ADHAALATH';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'JP';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'MDA';
        $party->save();

        $party = new \App\PoliticalParty();
        $party->name = 'INDEPENDENT';
        $party->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('political_parties');
    }
}
