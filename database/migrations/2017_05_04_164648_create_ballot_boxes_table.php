<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallotBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ballot_boxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('name')->nullable();;
            $table->integer('island_id');
            $table->integer('atoll_id');
            $table->integer('valid_votes')->nullable();
            $table->integer('invalid_votes')->nullable();
            $table->integer('total_votes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ballot_boxes');
    }
}
