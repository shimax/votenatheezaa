<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityCouncilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_councils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('atoll_id');
            $table->timestamps();
        });

        $city = new \App\CityCouncil();
        $city->name = 'Male\' CIty';
        $city->atoll_id = 8;
        $city->save();

        $city = new \App\CityCouncil();
        $city->name = 'Addu City';
        $city->atoll_id = 20;
        $city->save();

        $city = new \App\CityCouncil();
        $city->name = 'Fuvahmulah City';
        $city->atoll_id = 19;
        $city->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_councils');
    }
}
