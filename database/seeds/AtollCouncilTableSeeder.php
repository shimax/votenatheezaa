<?php

use Illuminate\Database\Seeder;

class AtollCouncilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          ['id' => '1', 'name' => 'dhaairaa_name', 'atoll_id' => 1],
          ['id' => '2', 'name' => 'dhaairaa_name', 'atoll_id' => 2],
          ['id' => '3', 'name' => 'dhaairaa_name', 'atoll_id' => 3],
      ];

      foreach (range(0, sizeof($data) - 1) as $index) {
            try {
                $item = CityCouncil::firstOrNew(array('id' => $data[$index]['id']));

                $item->title = $data[$index]['name'];
                $item->title = $data[$index]['atoll_id'];

                $item->save();
            } catch (Illuminate\Database\QueryException $excp) {
                echo "Not working." . PHP_EOL;
            }
        }
    }
    }
}
