<?php

use Illuminate\Database\Seeder;
use App\Atoll;
class AtollTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          ['id' => '1', 'name' => 'Haa Alif', 'official_atoll_name' => 'Thiladhunmathi Uthuruburi'],
          ['id' => '2', 'name' => 'Haa Dhaalu', 'official_atoll_name' => 'Thiladhunmathi Dhekunuburi'],
          ['id' => '3', 'name' => 'Shaviyani', 'official_atoll_name' => 'Miladhunmadulu Uthuruburi'],
          ['id' => '4', 'name' => 'Noonu', 'official_atoll_name' => 'Miladhunmadulu Dhekunuburi'],
          ['id' => '5', 'name' => 'Raa', 'official_atoll_name' => 'Maalhosmadulu Uthuruburi'],
          ['id' => '6', 'name' => 'Baa', 'official_atoll_name' => 'Maalhosmadulu Dhekunuburi'],
          ['id' => '7', 'name' => 'Lhaviyani', 'official_atoll_name' => 'Faadhippolhu'],
          ['id' => '8', 'name' => 'Kaafu', 'official_atoll_name' => 'Malé Atholhu'],
          ['id' => '9', 'name' => 'Alif Alif', 'official_atoll_name' => 'Ari Atholhu Uthuruburi'],
          ['id' => '10', 'name' => 'Alif Dhaal', 'official_atoll_name' => 'Ari Atholhu Dhekunuburi'],
          ['id' => '11', 'name' => 'Vaavu', 'official_atoll_name' => 'Felidhu Atholhu'],
          ['id' => '12', 'name' => 'Meemu', 'official_atoll_name' => 'Mulak Atholhu'],
          ['id' => '13', 'name' => 'Faafu', 'official_atoll_name' => 'Nilandhe Atholhu Uthuruburi'],
          ['id' => '14', 'name' => 'Dhaalu', 'official_atoll_name' => 'Nilandhe Atholhu Dhekunuburi'],
          ['id' => '15', 'name' => 'Thaa', 'official_atoll_name' => 'Kolhumadulu'],
          ['id' => '16', 'name' => 'Laamu', 'official_atoll_name' => 'Haddhunmathi'],
          ['id' => '17', 'name' => 'Gaafu Alif', 'official_atoll_name' => 'Huvadhu Atholhu Uthuruburi'],
          ['id' => '18', 'name' => 'Gaafu Dhaalu', 'official_atoll_name' => 'Huvadhu Atholhu Dhekunuburi'],
          ['id' => '19', 'name' => 'Gnaviyani', 'official_atoll_name' => 'Fuvahmulah'],
          ['id' => '20', 'name' => 'Seenu', 'official_atoll_name' => 'Addu City'],
          ['id' => '21', 'name' => 'Male\'', 'official_atoll_name' => 'Male\' City'],
      ];

      foreach (range(0, sizeof($data) - 1) as $index) {
            try {
                $item = Atoll::firstOrNew(array('id' => $data[$index]['id']));
                $item->title = $data[$index]['name'];
                $item->title = $data[$index]['official_atoll_name'];

                $item->save();
            } catch (Illuminate\Database\QueryException $excp) {
                echo "Not working." . PHP_EOL;
            }
        }

    }
}
