<?php

use Illuminate\Database\Seeder;
use App\CityCouncil;
class CityCouncilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          ['id' => '1', 'name' => 'Male\' City'],
          ['id' => '2', 'name' => 'Addu City'],
          ['id' => '3', 'name' => 'Fuvahmulah City'],
      ];

      foreach (range(0, sizeof($data) - 1) as $index) {
            try {
                $item = CityCouncil::firstOrNew(array('id' => $data[$index]['id']));

                $item->title = $data[$index]['name'];

                $item->save();
            } catch (Illuminate\Database\QueryException $excp) {
                echo "Not working." . PHP_EOL;
            }
        }
    }
}
