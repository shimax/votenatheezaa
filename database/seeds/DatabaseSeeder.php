<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AtollTableSeeder::class);
        $this->call(IslandTypeTableSeeder::class);
        $this->call(IslandTableSeeder::class);
        $this->call(CityCouncilTableSeeder::class);
        $this->call(AtollCouncilTableSeeder::class);        
    }
}
