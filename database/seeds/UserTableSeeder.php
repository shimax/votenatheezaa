<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();

      User::create([
          'username' => 'admin',
          'email' => 'admin@hype.com',
          'password' => Hash::make('secret')
      ]);

      User::create([
          'username' => 'miuvaan',
          'email' => 'miuvaan@hype.com',
          'password' => Hash::make('password@123')
      ]);

      User::create([
          'username' => 'maxipy',
          'email' => 'spyhacker@hype.com',
          'password' => Hash::make('password@123')
      ]);
    }
}
