@extends('layouts.chart')
@section('content')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Number of Votes", "Candidates", { role: "style" },{ role: 'annotation' }  ],

      <?php
        echo $string;
//            dd($string);
       ?>

    ]);


    var view = new google.visualization.DataView(data);
    var options = {
//      title: "Local Council 2017",
      width: 1100,
      height: 580,
      legendTextStyle: { color: '#FFF' },
  titleTextStyle: { color: '#FFF' , fontSize:"32", textAlign: "center"},
      legend: { position: "none" },
      backgroundColor: 'transparent',
      // backgroundColor: "#f8f8f9",
      hAxis: {title: "Candidates",  textStyle:{color: '#FFF', fontSize:"18"},titleTextStyle: { color: '#FFF' , fontSize:"18"}},
      vAxis: {title:"Number of Votes" ,viewWindowMode: "explicit",   textStyle:{color: '#FFF', fontSize:"18"},titleTextStyle: { color: '#FFF' , fontSize:"18"}, viewWindow:{ min: 0 }},
      animation: {
        duration: 1000,
        startup:true,
      }

    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}
    </script>




    <div class="container"  style="margin-top:10%;">
      <div class="row">
        <!-- <div class="col-md-1"></div> -->
        <div style=" text-align: center; color: #FFF"><h1>{{ $tesBallox->name }} </h1></div>


        <div class="col-md-10" >
          <div id="columnchart_values" style="width: 1000px; height: 800px; "></div>

        </div>
        <div class="col-md-2">
          <br><br><br>  <br><br><br>  <br><br><br>
          <table style="color:white;">


          <tr>
          <td><div style="background-color:purple; width:20px; height:20px;"></div></td>
          <td>&nbsp;&nbsp; PPM </td>
          </tr>
          <tr>
          <td><div style="background-color:yellow; width:20px; height:20px;"></td>
          <td>&nbsp;&nbsp; MDP </td>
          </tr>
          <tr>
          <td><div style="background-color:red; width:20px; height:20px;"></td>
          <td>&nbsp;&nbsp; JP </td>
          </tr>
          <tr>
          <td><div style="background-color:orange; width:20px; height:20px;"></td>
          <td>&nbsp;&nbsp; MDA </td>
          </tr>
          <tr>
          <td><div style="background-color:blue; width:20px; height:20px;"></td>
          <td>&nbsp;&nbsp; DRP </td>
          </tr>
          <tr>
          <td><div style="background-color:gray; width:20px; height:20px;"></td>
          <td>&nbsp;&nbsp; INDEPENDENT </td>
          </tr>

          </table>
        </div>
      </div>
    </div>

<!-- <div class="container" style="margin-top:20%;margin-left:23%;">
  <div id="columnchart_values" style="width: 900px; height: 800px;"></div>


</div> -->
<!--
<div class="container" style="margin-top:20%;">
<div class="row">
  <div class="col-md-6">
    <div id="columnchart_values" style="width: 900px; height: 800px;"></div>

  </div>


  <div class="col-md-6">

    <br><br><br><br>
    <table style="width:100%">


  <tr>
    <td><img src="{{ asset('img/ppm_logo.png')}}" alt="Smiley face"  width="80"></td>
    <td> Candidte Description </td>
  </tr>

  <tr>
    <td>Candidate Title</td>
    <td> Candidte Description </td>
  </tr>

  <tr>
    <td>Candidate Title</td>
    <td> Candidte Description </td>
  </tr>

  <tr>
    <td>Candidate Title</td>
    <td> Candidte Description </td>
  </tr>
</table>

  </div>
</div>
</div> -->


@endsection
