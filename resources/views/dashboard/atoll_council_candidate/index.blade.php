@extends('layouts.app')

@section('content')


<h1> Add Atoll Council Candidate </h1>
{{ Form::open(['action' => 'AtollCouncilCandidateController@store'])  }}
{{ csrf_field()  }}


<div class="form-group">
  <label for="name"> Atoll </label>
  {{ Form::select('atoll_council_section_id',$atolls->pluck('english_atoll_name_long','id'), null,['class' => "form-control"]) }}

</div>

<div class="form-group">
  <label for="name"> Number</label>
  <input type="text" name="number"class="form-control" >
</div>

<div class="form-group">
  <label for="name"> Name</label>
  <input type="text" name="name"class="form-control" >
</div>



<div class="form-group">
  <label for="name"> Political Party </label>
  {{ Form::select('political_party_id',$political_parties->pluck('name','id'), null,['class' => "form-control"]) }}

</div>


<input type="submit" class="btn btn-primary">
{{Form::close() }}



@if($atoll_council_candidates!= null)

<h3> Existing Atoll Council Candidates </h3>

<table class="table " id="datatable">
    <thead>
    <th> Name</th><th>Number</th><th>Party</th><th>Atoll</th><th>Delete</th>
  </thead>
  <tbody>
    @foreach($atoll_council_candidates as $atoll_council_candidate)
    <tr>
      <td> {{$atoll_council_candidate->candidate->name}} </td>
      <td> {{$atoll_council_candidate->candidate->number}} </td>
      <td> {{$atoll_council_candidate->candidate->political_party->name}} </td>
      <td> {{$atoll_council_candidate->atoll_council->atoll->english_atoll_name_long}} </td>
      <td>
        {!! Form::open(['action' => ['AtollCouncilCandidateController@destroy', $atoll_council_candidate->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
