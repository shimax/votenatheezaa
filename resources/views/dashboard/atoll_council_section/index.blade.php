@extends('layouts.app')

@section('content')


<h1> Add Dhaairaa </h1>
{{ Form::open(['action' => 'AtollCouncilSectionController@store'])  }}
{{ csrf_field()  }}


<div class="form-group">
  <label for="name"> Atoll </label>
  {{ Form::select('atoll_council_id',$atolls->pluck('english_atoll_name_long','id'), null,['class' => "form-control"]) }}

</div>

<div class="form-group">
  <label for="name"> Name</label>
  <input type="text" name="name"class="form-control" >
</div>


<input type="submit" class="btn btn-primary">
{{Form::close() }}



@if($atoll_council_sections!= null)

<h3> Existing Dhaairaa'tha </h3>

<table class="table"  id="datatable">
    <thead>
    <th>Atoll</th><th> Name</th><th>Delete</th>
  </thead>
  <tbody>
    @foreach($atoll_council_sections as $atoll_council_section)
    <tr>
      <td> {{$atoll_council_section->atoll_council->atoll->english_atoll_name_long}} </td>
      <td> {{$atoll_council_section->name}} </td>
      <td>
        {!! Form::open(['action' => ['AtollCouncilSectionController@destroy', $atoll_council_section->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
