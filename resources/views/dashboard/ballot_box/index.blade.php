@extends('layouts.app')

@section('content')


<h1> Add Ballot Box </h1>
{{ Form::open(['action' => 'BallotBoxController@store'])  }}
{{ csrf_field()  }}


<div class="form-group">
  <label for="name"> Island </label>
  {{ Form::select('island_id',$formatted_islands->pluck('name','id'), null,['class' => "form-control select2"]) }}

</div>


<div class="form-group">
  <label for="name"> Dhaairaa</label>
  {{ Form::select('atoll_council_section_id',$atoll_council_sections->pluck('name','id'), null,['class' => "form-control select2"]) }}
</div>


<div class="form-group">
  <label for="name"> Number</label>
  <input type="text" name="number"class="form-control" >
</div>
<div class="form-group">
  <label for="name"> Name</label>
  <input type="text" name="name"class="form-control" >
</div>
<div class="form-group">
  <label for="location"> Location</label>
  <input type="text" name="location"class="form-control" >
</div>

<input type="submit" class="btn btn-primary">
{{Form::close() }}



@if($ballot_boxes!= null)

<h3> Existing Ballot Boxes </h3>

<table class="table" id="datatable">
    <thead>
    <th> Number</th> <th> Name </th> <th>Location</th> <th>Atoll</th> <th>Island</th> <th>Dhaairaa</th> <th>Valid Votes</th><th>Invalid Votes</th><th>Total Votes</th><th>Delete</th>
  </thead>
  <tbody>
    @foreach($ballot_boxes as $ballot_box)
    <tr>
      <td> <a href="{{action('BallotBoxController@show',$ballot_box->id)}}" ><b>{{$ballot_box->number}} </b> </a> </td>
      <td> {{$ballot_box->name or ''}} </td>
    <td> {{$ballot_box->location or ' '}} </td>
      <td> {{$ballot_box->atoll->english_atoll_name_long}} </td>
      <td> {{$ballot_box->island->english_official_island_name}} </td>

      <td>

        @if($ballot_box->atoll_council_section!= null)

        {{$ballot_box->atoll_council_section->name}}
@endif

      </td>

      <td> {{$ballot_box->valid_votes}} </td>
      <td> {{$ballot_box->invalid_votes}} </td>
      <td> {{$ballot_box->total_votes}} </td>
      <td>
        {!! Form::open(['action' => ['BallotBoxController@destroy', $ballot_box->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
