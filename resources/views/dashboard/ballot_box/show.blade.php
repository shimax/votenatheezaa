@extends('layouts.app')

@section('content')





<h1>  <a href="{{ action('BallotBoxController@index') }} "> Ballot Box </a> > Details</h1>
<h3> Name: {{$ballot_box->name}} , Total Votes:  {{$ballot_box->total_votes}} , Valid Votes: {{$ballot_box->valid_votes}} , Invalid Votes: {{$ballot_box->invalid_votes}}</h3>

<div class=" pull-right">
<a href="{{action('ChartController@display_ballot_box_of_atoll_council', $ballot_box->id)}}" target="_new"><button type="btn btn-primary "> Display Atoll Council Chart </button> </a>
  <a href="{{action('ChartController@display_ballot_box_of_island_council', $ballot_box->id)}}" target="_new"><button type="btn btn-primary "> Display Island Council Chart </button> </a>

</div>


<br>
{{ Form::open(['action' => ['BallotBoxController@update',$ballot_box->id], 'method' => 'PUT'])  }}
{{ csrf_field()  }}


<div class="form-group">
  <label for="name"> Valid Votes </label>
  <input type="text" name="valid_votes"class="form-control" >

</div>


<div class="form-group">
  <label for="name"> Invalid Votes</label>
  <input type="text" name="invalid_votes"class="form-control" >
</div>

<input type="submit" class="btn btn-primary" value="Update">
{{Form::close() }}

<br>

@if($ballot_box->details!= null)


<table class="table" id="groupeddatatable">
    <thead>
  <th> Type </th>   <th> Candidate Number</th><th> Candidate Name</th> <th>Party</th><th>Atoll</th> <th>Island</th> <th>Dhaairaa </th> <th> Votes</th><th>Update</th>
  <th>Delete</th>
  </thead>
  <tbody>
    @foreach($ballot_box->details as $detail)
    <tr>
      <td> @if($detail->council_type_id == 1)
        ATOLL COUNCIL @elseif($detail->council_type_id == 2) ISLAND COUNCIL @else CITY COUNCIL @endif
      </td>

      <td>{{$detail->candidate->number}} </td>
      <td> <a href="{{action('BallotBoxController@show',$ballot_box->id)}}" ><b>{{$detail->candidate->name}} </b> </a> </td>
      <td>{{$detail->candidate->political_party->name}} </td>

      <td> {{$ballot_box->atoll->english_atoll_name_long}} </td>
      <td> {{$ballot_box->island->english_official_island_name}} </td>
      <td>

        @if($detail->dhaairaa!= null)

        {{$detail->dhaairaa->name}}

        @endif
      </td>
      <td> {{$detail->votes}} </td>

  <td>
    {!! Form::open(['action' => ['BallotBoxDetailController@update', $detail->id],  'method' => 'patch']) !!}
    {{ csrf_field()  }}
    <input type="text" name="votes"class="form-control" placeholder="Votes" >
    <button type="submit" class="btn btn-default btn-block"  style="display:inline-block;" >Update</button>
    {{Form::close() }}
</td>
  <td>
        {!! Form::open(['action' => ['BallotBoxDetailController@destroy', $detail->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
