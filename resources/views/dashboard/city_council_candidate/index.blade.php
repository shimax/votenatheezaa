@extends('layouts.app')

@section('content')


    <h1> Add City Council Candidate </h1>
    {{ Form::open(['action' => 'CityCouncilCandidateController@store'])  }}
    {{ csrf_field()  }}


    <div class="form-group">
        <label for="name"> City </label>
        {{ Form::select('city_council_section_id',$city_council_sections->pluck('name','id'), null,['class' => "form-control select2"]) }}

    </div>

    <div class="form-group">
        <label for="name"> Number</label>
        <input type="text" name="number"class="form-control" >
    </div>

    <div class="form-group">
        <label for="name"> Name</label>
        <input type="text" name="name"class="form-control" >
    </div>



    <div class="form-group">
        <label for="name"> Political Party </label>
        {{ Form::select('political_party_id',$political_parties->pluck('name','id'), null,['class' => "form-control"]) }}

    </div>


    <input type="submit" class="btn btn-primary">
    {{Form::close() }}



    @if($city_council_candidates!= null)
        <h3> Existing Atoll Council Candidates </h3>

        <table class="table" id="datatable">
            <thead>
            <th> City </th><th> Dhaairaa Name </th><th> Number</th><th>Name</th><th>Party</th><th>Delete</th>
            </thead>
            <tbody>
            @foreach($city_council_candidates as $city_council_candidate)
                <tr>
                    <td> {{$city_council_candidate->city_council_section->city_council->name}} </td>
                    <td> {{$city_council_candidate->city_council_section->name}} </td>
                    <td> {{$city_council_candidate->candidate->number}} </td>
                    <td> {{$city_council_candidate->candidate->name}} </td>
                    <td> {{$city_council_candidate->candidate->political_party->name}} </td>
                    <td>
                        {!! Form::open(['action' => ['CityCouncilCandidateController@destroy', $city_council_candidate->id],  'method' => 'delete']) !!}
                        {{ csrf_field()  }}
                        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
                        {{Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    @endif


@endsection
