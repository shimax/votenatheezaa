@extends('layouts.app')

@section('content')


<h1> Add Custom Ballot Box </h1>
{{ Form::open(['action' => 'CustomBallotBoxController@store'])  }}
{{ csrf_field()  }}



<div class="form-group">
  <label for="name"> Number</label>
  <input type="text" name="number"class="form-control" >
</div>
<div class="form-group">
  <label for="name"> Name</label>
  <input type="text" name="name"class="form-control" >
</div>
<div class="form-group">
  <label for="location"> Location</label>
  <input type="text" name="location"class="form-control" >
</div>

<input type="submit" class="btn btn-primary">
{{Form::close() }}



@if($custom_ballot_boxes!= null)

<h3> Existing Ballot Boxes </h3>

<table class="table" id="datatable">
    <thead>
    <th> Number</th> <th> Name </th> <th>Location</th> <th>Valid Votes</th><th>Invalid Votes</th><th>Total Votes</th><th>Delete</th>
  </thead>
  <tbody>
    @foreach($custom_ballot_boxes as $custom_ballot_box)
    <tr>
      <td> <a href="{{action('CustomBallotBoxController@show',$custom_ballot_box->id)}}" ><b>{{$custom_ballot_box->number}} </b> </a> </td>
      <td> {{$custom_ballot_box->name or ''}} </td>
    <td> {{$custom_ballot_box->location or ' '}} </td>



      <td> {{$custom_ballot_box->valid_votes}} </td>
      <td> {{$custom_ballot_box->invalid_votes}} </td>
      <td> {{$custom_ballot_box->total_votes}} </td>
      <td>
        {!! Form::open(['action' => ['CustomBallotBoxController@destroy', $custom_ballot_box->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
