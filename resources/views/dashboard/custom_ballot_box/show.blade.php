@extends('layouts.app')

@section('content')





<h1>  <a href="{{ action('CustomBallotBoxController@index') }} "> Custom Ballot Box </a> > Details</h1>
<h3> Name: {{$custom_ballot_box->name}} , Total Votes:  {{$custom_ballot_box->total_votes}} , Valid Votes: {{$custom_ballot_box->valid_votes}} , Invalid Votes: {{$custom_ballot_box->invalid_votes}}</h3>

<div class=" pull-right">
<a href="{{action('ChartController@display_custom_ballot_box', $custom_ballot_box->id)}}" target="_new"><button type="btn btn-primary "> Display Custom Chart </button> </a>
</div>


<br>
{{ Form::open(['action' => ['CustomBallotBoxController@update',$custom_ballot_box->id], 'method' => 'PUT'])  }}
{{ csrf_field()  }}


<div class="form-group">
  <label for="name"> Valid Votes </label>
  <input type="text" name="valid_votes"class="form-control" >

</div>


<div class="form-group">
  <label for="name"> Invalid Votes</label>
  <input type="text" name="invalid_votes"class="form-control" >
</div>

<input type="submit" class="btn btn-primary" value="Update">
{{Form::close() }}

<br>

{{ Form::open(['action' => ['CustomBallotBoxController@add_candidate'], 'method' => 'POST'])  }}
{{ csrf_field()  }}

<input type="hidden" name="custom_ballot_box_id" value="{{$custom_ballot_box->id}}">

<div class="form-group">
  <label for="name"> Candidates </label>
  {{ Form::select('candidate_id',$all_candidates->pluck('name','id'), null,['class' => "form-control select2"]) }}

</div>


<div class="form-group">
  <label for="name">Votes</label>
  <input type="text" name="votes"class="form-control" >
</div>

<input type="submit" class="btn btn-primary" value="Update">
{{Form::close() }}


</br>

@if($custom_ballot_box->details!= null)


<table class="table" id="datatable">
    <thead>
  <th> Type </th>   <th> Candidate Number</th><th> Candidate Name</th> <th>Party</th>  <th> Votes</th><th>Update</th>
  <th>Delete</th>
  </thead>
  <tbody>
    @foreach($custom_ballot_box->details as $detail)
    <tr>
      <td> @if($detail->council_type_id == 1)
        ATOLL COUNCIL @elseif($detail->council_type_id == 2) ISLAND COUNCIL @else CITY COUNCIL @endif
      </td>

      <td>{{$detail->candidate->number}} </td>
      <td> <a href="{{action('CustomBallotBoxController@show',$custom_ballot_box->id)}}" ><b>{{$detail->candidate->name}} </b> </a> </td>
      <td>{{$detail->candidate->political_party->name}} </td>


      <td> {{$detail->votes}} </td>

  <td>
    {!! Form::open(['action' => ['CustomBallotBoxDetailController@update', $detail->id],  'method' => 'patch']) !!}
    {{ csrf_field()  }}
    <input type="text" name="votes"class="form-control" placeholder="Votes" >
    <button type="submit" class="btn btn-default btn-block"  style="display:inline-block;" >Update</button>
    {{Form::close() }}
</td>
  <td>
        {!! Form::open(['action' => ['CustomBallotBoxDetailController@destroy', $detail->id],  'method' => 'delete']) !!}
        {{ csrf_field()  }}
        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
        {{Form::close() }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


@endif


@endsection
