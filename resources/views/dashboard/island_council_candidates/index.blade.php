@extends('layouts.app')

@section('content')


    <h1> Add Island Council Candidate </h1>
    {{ Form::open(['action' => 'IslandCouncilCandidateController@store'])  }}
    {{ csrf_field()  }}


    <div class="form-group">
        <label for="name"> Island </label>
        {{ Form::select('island_id',$islands->pluck('name','id'), null,['class' => "form-control select2"]) }}

    </div>


    <div class="form-group">
        <label for="name"> Number</label>
        <input type="text" name="number" class="form-control" >
    </div>

    <div class="form-group">
        <label for="name"> Name</label>
        <input type="text" name="name" class="form-control" >
    </div>

    <div class="form-group">
        <label for="name"> Political Party </label>
        {{ Form::select('political_party_id',$political_parties->pluck('name','id'), null,['class' => "form-control select2"]) }}

    </div>


    <input type="submit" class="btn btn-primary">
    {{Form::close() }}


    @if($island_council_candidates!= null)

        <h3> Existing Island Council Candidates </h3>

        <table class="table" id="datatable">
            <thead>
            <th>Atoll</th><th>Island</th><th>Number</th><th> Name</th><th>Party</th><th>Delete</th>
            </thead>
            <tbody>
            @foreach($island_council_candidates as $island_council_candidate)
                <tr>
                    <td> {{$island_council_candidate->atoll->english_atoll_name_long}} </td>
                    <td> {{$island_council_candidate->island->english_official_island_name}} </td>
                    <td> {{$island_council_candidate->candidate->number}} </td>
                    <td> {{$island_council_candidate->candidate->name}} </td>
                    <td> {{$island_council_candidate->candidate->political_party->name}} </td>
                    <td>
                        {!! Form::open(['action' => ['IslandCouncilCandidateController@destroy', $island_council_candidate->id],  'method' => 'delete']) !!}
                        {{ csrf_field()  }}
                        <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
                        {{Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    @endif


@endsection
