<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Conuncil Inthihaabu Results</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/w3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css"/>



    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <style>
    .mdp{
      background-color:rgba(248, 195, 1, 0.19);
    }

    .ppm{
      background-color: #ffe0f0;
    }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Council Elections
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">

                      <li><a href="{{ action('AtollCouncilSectionController@index') }}">Dhaairaa</a></li>
                      <li><a href="{{ action('AtollCouncilCandidateController@index') }}">Atoll Council Candidates</a></li>
                      <li><a href="{{ action('IslandCouncilCandidateController@index') }}">Island Council Candidates</a></li>
                      <li><a href="{{ action('CityCouncilCandidateController@index') }}">City Council Candidates</a></li>
                      <li><a href="{{ action('BallotBoxController@index') }}">Ballot Box</a></li>
                      <li><a href="{{ action('CustomBallotBoxController@index') }}">Custom Ballot Box</a></li>

                        <!-- Authentication Links -->
                        @if (Auth::guest())


                            <li><a href="{{ action('AtollCouncilSectionController@index') }}">Dhaairaa</a></li>
                            <li><a href="{{ action('AtollCouncilCandidateController@index') }}">Atoll Council Candidates</a></li>
                            <li><a href="{{ action('IslandCouncilCandidateController@index') }}">Island Council Candidates</a></li>
                            <li><a href="{{ action('CityCouncilCandidateController@index') }}">City Council Candidates</a></li>
                            <li><a href="{{ action('BallotBoxController@index') }}">Ballot Box</a></li>
                            <li><a href="{{ action('CustomBallotBoxController@index') }}">Custom Ballot Box</a></li>


                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

<div class="container">
        @yield('content')
</div>
    </div>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>



    <script>
    //   $(document).ready(function() {
    // });


    $(document).ready(function() {
    $('#groupeddatatable').DataTable( {
        orderFixed: [0, 'asc'],
        rowGroup: {
            dataSrc: 0
        },
        "createdRow": function( row, data, dataIndex ) {
          if ( data[3] == "MDP" ) {
            $(row).addClass( 'mdp' );
          }
          if ( data[3] == "PPM" ) {
            $(row).addClass( 'ppm' );
          }

        }
    } );

        $('#datatable').DataTable();

$(".select2").select2();

} );
  </script>
</body>
</html>
