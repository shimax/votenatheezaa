<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
    //::::::::::Front-end-routes::::::::::/
Route::get('/atoll-council-overall', 'HomeController@atollCouncilOverall');

Route::get('/ballot-box-atoll-council/{bbid}','ChartController@display_ballot_box_of_atoll_council');

Route::get('/ballot-box-city-council/{bbid}','ChartController@display_ballot_box_of_city_council');
Route::get('/ballot-box-island-council/{bbid}','ChartController@display_ballot_box_of_island_council');

Route::get('/custom-ballot-box-chart/{bbid}','ChartController@display_custom_ballot_box');



Route::get('/ballot-box-island-council/{bbid}','ChartController@display_ballot_box_of_island_council');

    //::::::::::Front-end-routes::::::::::/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
  Route::get('/home', 'HomeController@index')->name('home');
  Route::resource('/atoll-council-section', 'AtollCouncilSectionController');



  Route::resource('/atoll_council_candidates', 'AtollCouncilCandidateController');

  Route::resource('/island_council_candidates', 'IslandCouncilCandidateController');

  Route::resource('/city_council_candidates', 'CityCouncilCandidateController');

  Route::resource('/ballot-box', 'BallotBoxController');
  Route::resource('/ballot-box-detail', 'BallotBoxDetailController');
  Route::post('/add-custom-ballot-box-candidate', 'CustomBallotBoxController@add_candidate');

  Route::resource('/custom-ballot-box', 'CustomBallotBoxController');
  Route::resource('/custom-ballot-box-detail', 'CustomBallotBoxDetailController');

});
