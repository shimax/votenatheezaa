-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2017 at 09:04 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `councilelections`
--

-- --------------------------------------------------------

--
-- Table structure for table `atolls`
--

CREATE TABLE `atolls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_atoll_name_long` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_atoll_name_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_official_atoll_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_atoll_name_long` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_atoll_name_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_official_atoll_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atolls`
--

INSERT INTO `atolls` (`id`, `name`, `reg_no`, `english_atoll_name_long`, `english_atoll_name_short`, `english_official_atoll_name`, `dhivehi_atoll_name_long`, `dhivehi_atoll_name_short`, `dhivehi_official_atoll_name`, `created_at`, `updated_at`) VALUES
(1, 'A', 'A', 'Haa Alif', 'HA', 'Thiladhunmathi Uthuruburi', '?? ??????', '??', '???????????? ??????????', NULL, NULL),
(2, 'B', 'B', 'Haa Dhaalu', 'HDh', 'Thiladhunmathi Dhekunuburi', '?? ????', '??', '???????????? ??????????', NULL, NULL),
(3, 'C', 'C', 'Shaviyani', 'Sh', 'Miladhunmadulu Uthuruburi', '????????', '?', '?????????????? ??????????', NULL, NULL),
(4, 'D', 'D', 'Noonu', 'N', 'Miladhunmadulu Dhekunuburi', '????', '?', '?????????????? ??????????', NULL, NULL),
(5, 'E', 'E', 'Raa', 'R', 'Maalhosmadulu Uthuruburi', '??', '?', '???????????? ??????????', NULL, NULL),
(6, 'F', 'F', 'Baa', 'B', 'Maalhosmadulu Dhekunuburi', '??', '?', '???????????? ??????????', NULL, NULL),
(7, 'G', 'G', 'Lhaviyani', 'Lh', 'Faadhippolhu', '????????', '?', '??????????', NULL, NULL),
(8, 'H', 'H', 'Kaafu', 'K', 'Mal? Atholhu', '????', '?', '???? ??????', NULL, NULL),
(9, 'U', 'U', 'Alif Alif', 'AA', 'Ari Atholhu Uthuruburi', '?????? ??????', '??', '?????????? ??????????', NULL, NULL),
(10, 'I', 'I', 'Alif Dhaal', 'Adh', 'Ari Atholhu Dhekunuburi', '?????? ????', '??', '?????????? ??????????', NULL, NULL),
(11, 'J', 'J', 'Vaavu', 'V', 'Felidhu Atholhu', '????', '?', '?????? ??????', NULL, NULL),
(12, 'K', 'K', 'Meemu', 'M', 'Mulak Atholhu', '????', '?', '?????? ??????', NULL, NULL),
(13, 'L', 'L', 'Faafu', 'F', 'Nilandhe Atholhu Uthuruburi', '????', '?', '???????? ?????? ??????????', NULL, NULL),
(14, 'M', 'M', 'Dhaalu', 'Dh', 'Nilandhe Atholhu Dhekunuburi', '????', '?', '???????? ?????? ??????????', NULL, NULL),
(15, 'N', 'N', 'Thaa', 'Th', 'Kolhumadulu', '??', '?', '??????????', NULL, NULL),
(16, 'O', 'O', 'Laamu', 'L', 'Haddhunmathi', '????', '?', '????????????', NULL, NULL),
(17, 'P', 'P', 'Gaafu Alif', 'GA', 'Huvadhu Atholhu Uthuruburi', '???? ??????', '??', '?????? ?????? ??????????', NULL, NULL),
(18, 'Q', 'Q', 'Gaafu Dhaalu', 'GDh', 'Huvadhu Atholhu Dhekunuburi', '???? ????', '??', '?????? ?????? ??????????', NULL, NULL),
(19, 'R', 'R', 'Gnaviyani', 'Gn', 'Fuvahmulah', '????????', '?', '????????????', NULL, NULL),
(20, 'S', 'S', 'Seenu', 'S', 'Addu City', '????', '?', '?????? ????', NULL, NULL),
(21, 'T', 'T', 'Male''', 'Male''', 'Male'' City', '????', '', '???? ????', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `atoll_councils`
--

CREATE TABLE `atoll_councils` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_councils`
--

INSERT INTO `atoll_councils` (`id`, `atoll_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(6, 6, NULL, NULL),
(7, 7, NULL, NULL),
(8, 8, NULL, NULL),
(9, 9, NULL, NULL),
(10, 10, NULL, NULL),
(11, 11, NULL, NULL),
(12, 12, NULL, NULL),
(13, 13, NULL, NULL),
(14, 14, NULL, NULL),
(15, 15, NULL, NULL),
(16, 16, NULL, NULL),
(17, 17, NULL, NULL),
(18, 18, NULL, NULL),
(19, 19, NULL, NULL),
(20, 20, NULL, NULL),
(21, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_sections`
--

CREATE TABLE `atoll_council_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_council_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_council_sections`
--

INSERT INTO `atoll_council_sections` (`id`, `atoll_council_id`, `name`, `created_at`, `updated_at`) VALUES
(4, 1, 'Ihavandhoo', '2017-05-04 11:08:51', '2017-05-04 11:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_ballot_boxes`
--

CREATE TABLE `atoll_council_section_ballot_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_ballot_box_details`
--

CREATE TABLE `atoll_council_section_ballot_box_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_section_ballot_box_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `total_votes` int(11) NOT NULL,
  `valid_votes` int(11) NOT NULL,
  `invalid_votes` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_candidates`
--

CREATE TABLE `atoll_council_section_candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_council_section_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_council_section_candidates`
--

INSERT INTO `atoll_council_section_candidates` (`id`, `atoll_council_section_id`, `candidate_id`, `created_at`, `updated_at`) VALUES
(3, 4, 6, '2017-05-04 11:33:10', '2017-05-04 11:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `ballot_boxes`
--

CREATE TABLE `ballot_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `island_id` int(11) NOT NULL,
  `atoll_id` int(11) NOT NULL,
  `valid_votes` int(11) DEFAULT NULL,
  `invalid_votes` int(11) DEFAULT NULL,
  `total_votes` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ballot_boxes`
--

INSERT INTO `ballot_boxes` (`id`, `number`, `island_id`, `atoll_id`, `valid_votes`, `invalid_votes`, `total_votes`, `created_at`, `updated_at`) VALUES
(1, '007', 1, 1, NULL, NULL, NULL, '2017-05-04 12:34:54', '2017-05-04 12:34:54');

-- --------------------------------------------------------

--
-- Table structure for table `ballot_box_details`
--

CREATE TABLE `ballot_box_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `council_type_id` int(11) DEFAULT NULL,
  `votes` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `political_party_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `name`, `number`, `political_party_id`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed', '02', 1, '2017-05-04 10:53:55', '2017-05-04 10:53:55'),
(2, 'Shimad Firaaq', '03', 1, '2017-05-04 10:54:29', '2017-05-04 10:54:29'),
(3, 'Shimad Firaaq', '03', 1, '2017-05-04 10:54:49', '2017-05-04 10:54:49'),
(4, 'Shimad Firaaq', '03', 1, '2017-05-04 10:55:11', '2017-05-04 10:55:11'),
(5, 'Mohammed Aboobakuru', '03', 2, '2017-05-04 10:57:23', '2017-05-04 10:57:23'),
(6, 'Shimad MOhammed', '03', 1, '2017-05-04 11:33:10', '2017-05-04 11:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `city_councils`
--

CREATE TABLE `city_councils` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `islands`
--

CREATE TABLE `islands` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `island_reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_official_island_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_official_island_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `island_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `islands`
--

INSERT INTO `islands` (`id`, `atoll_reg_no`, `island_reg_no`, `english_official_island_name`, `dhivehi_official_island_name`, `island_type`, `created_at`, `updated_at`) VALUES
(1, 'A', 'A01', 'Thuraakunu', '????????', 'Inhabited Island', NULL, NULL),
(2, 'A', 'A02', 'Uligan', '????????', 'Inhabited Island', NULL, NULL),
(3, 'A', 'A05', 'Molhadhoo', '??????', 'Inhabited Island', NULL, NULL),
(4, 'A', 'A06', 'Hoarafushi', '????????', 'Inhabited Island', NULL, NULL),
(5, 'A', 'A07', 'Ihavandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(6, 'A', 'A08', 'Kelaa', '????', 'Inhabited Island', NULL, NULL),
(7, 'A', 'A09', 'Vashafaru', '????????', 'Inhabited Island', NULL, NULL),
(8, 'A', 'A10', 'Dhidhdhoo', '??????', 'Inhabited Island', NULL, NULL),
(9, 'A', 'A11', 'Filladhoo', '????????', 'Inhabited Island', NULL, NULL),
(10, 'A', 'A12', 'Maarandhoo', '????????', 'Inhabited Island', NULL, NULL),
(11, 'A', 'A13', 'Thakandhoo', '????????', 'Inhabited Island', NULL, NULL),
(12, 'A', 'A14', 'Utheemu', '??????', 'Inhabited Island', NULL, NULL),
(13, 'A', 'A15', 'Muraidhoo', '????????', 'Inhabited Island', NULL, NULL),
(14, 'A', 'A16', 'Baarah', '??????', 'Inhabited Island', NULL, NULL),
(15, 'A', '', 'Alidhoo', '??????', 'Resort Island', NULL, NULL),
(16, 'A', '', 'Dhonakulhi', '????????', 'Resort Island', NULL, NULL),
(17, 'A', '', 'Manafaru', '????????', 'Resort Island', NULL, NULL),
(18, 'B', 'B01', 'Faridhoo', '??????', 'Inhabited Island', NULL, NULL),
(19, 'B', 'B02', 'Hanimaadhoo', '????????', 'Inhabited Island', NULL, NULL),
(20, 'B', 'B03', 'Finey', '????', 'Inhabited Island', NULL, NULL),
(21, 'B', 'B04', 'Naivaadhoo', '????????', 'Inhabited Island', NULL, NULL),
(22, 'B', 'B05', 'Hirimaradhoo', '??????????', 'Inhabited Island', NULL, NULL),
(23, 'B', 'B06', 'Nolhivaranfaru', '??????????????', 'Inhabited Island', NULL, NULL),
(24, 'B', 'B07', 'Nellaidhoo', '??????????', 'Inhabited Island', NULL, NULL),
(25, 'B', 'B08', 'Nolhivaram', '??????????', 'Inhabited Island', NULL, NULL),
(26, 'B', 'B09', 'Kurinbi', '???????', 'Inhabited Island', NULL, NULL),
(27, 'B', 'B10', 'Kunburudhoo', '?????????', 'Inhabited Island', NULL, NULL),
(28, 'B', 'B11', 'Kulhudhuffushi', '????????????', 'Inhabited Island', NULL, NULL),
(29, 'B', 'B12', 'Kumundhoo', '????????', 'Inhabited Island', NULL, NULL),
(30, 'B', 'B13', 'Neykurendhoo', '??????????', 'Inhabited Island', NULL, NULL),
(31, 'B', 'B14', 'Vaikaradhoo', '??????????', 'Inhabited Island', NULL, NULL),
(32, 'B', 'B15', 'Maavaidhoo', '????????', 'Inhabited Island', NULL, NULL),
(33, 'B', 'B16', 'Makunudhoo', '????????', 'Inhabited Island', NULL, NULL),
(34, 'C', 'C01', 'Kanditheemu', '?????????', 'Inhabited Island', NULL, NULL),
(35, 'C', 'C02', 'Noomaraa', '??????', 'Inhabited Island', NULL, NULL),
(36, 'C', 'C03', 'Goidhoo', '??????', 'Inhabited Island', NULL, NULL),
(37, 'C', 'C04', 'Feydhoo', '????', 'Inhabited Island', NULL, NULL),
(38, 'C', 'C05', 'Feevah', '??????', 'Inhabited Island', NULL, NULL),
(39, 'C', 'C06', 'Bilehffahi', '??????????', 'Inhabited Island', NULL, NULL),
(40, 'C', 'C07', 'Foakaidhoo', '????????', 'Inhabited Island', NULL, NULL),
(41, 'C', 'C08', 'Narudhoo', '??????', 'Inhabited Island', NULL, NULL),
(42, 'C', 'C09', 'Maakandoodhoo', '?????????', 'Inhabited Island', NULL, NULL),
(43, 'C', 'C10', 'Maroshi', '??????', 'Inhabited Island', NULL, NULL),
(44, 'C', 'C11', 'Lhaimagu', '????????', 'Inhabited Island', NULL, NULL),
(45, 'C', 'C12', 'Firubaidhoo', '???????????', 'Inhabited Island', NULL, NULL),
(46, 'C', 'C13', 'Komandoo', '????????', 'Inhabited Island', NULL, NULL),
(47, 'C', 'C14', 'Maaungoodhoo', '?????????', 'Inhabited Island', NULL, NULL),
(48, 'C', 'C15', 'Funadhoo', '??????', 'Inhabited Island', NULL, NULL),
(49, 'C', 'C16', 'Milandhoo', '????????', 'Inhabited Island', NULL, NULL),
(50, 'C', '', 'Vagaru', '??????', 'Resort Island', NULL, NULL),
(51, 'D', 'D01', 'Henbandhoo', '???????', 'Inhabited Island', NULL, NULL),
(52, 'D', 'D02', 'Kendhikolhudhoo', '???????????', 'Inhabited Island', NULL, NULL),
(53, 'D', 'D04', 'Maalhendhoo', '????????', 'Inhabited Island', NULL, NULL),
(54, 'D', 'D05', 'Kudafari', '????????', 'Inhabited Island', NULL, NULL),
(55, 'D', 'D06', 'Landhoo', '??????', 'Inhabited Island', NULL, NULL),
(56, 'D', 'D07', 'Maafaru', '??????', 'Inhabited Island', NULL, NULL),
(57, 'D', 'D08', 'Lhohi', '????', 'Inhabited Island', NULL, NULL),
(58, 'D', 'D09', 'Miladhoo', '??????', 'Inhabited Island', NULL, NULL),
(59, 'D', 'D10', 'Magoodhoo', '??????', 'Inhabited Island', NULL, NULL),
(60, 'D', 'D11', 'Manadhoo', '??????', 'Inhabited Island', NULL, NULL),
(61, 'D', 'D12', 'Holhudhoo', '??????', 'Inhabited Island', NULL, NULL),
(62, 'D', 'D13', 'Fodhdhoo', '??????', 'Inhabited Island', NULL, NULL),
(63, 'D', 'D14', 'Velidhoo', '??????', 'Inhabited Island', NULL, NULL),
(64, 'D', '', 'Kudafunafaru', '????????????', 'Resort Island', NULL, NULL),
(65, 'D', '', 'Medhafushi', '????????', 'Resort Island', NULL, NULL),
(66, 'E', 'E01', 'Alifushi', '????????', 'Inhabited Island', NULL, NULL),
(67, 'E', 'E02', 'Vaadhoo', '????', 'Inhabited Island', NULL, NULL),
(68, 'E', 'E03', 'Rasgetheemu', '??????????', 'Inhabited Island', NULL, NULL),
(69, 'E', 'E04', 'Angolhitheemu', '???????????', 'Inhabited Island', NULL, NULL),
(70, 'E', 'E06', 'Ungoofaaru', '?????????', 'Inhabited Island', NULL, NULL),
(71, 'E', 'E08', 'Maakurathu', '????????', 'Inhabited Island', NULL, NULL),
(72, 'E', 'E09', 'Rasmaadhoo', '????????', 'Inhabited Island', NULL, NULL),
(73, 'E', 'E10', 'Innamaadhoo', '??????????', 'Inhabited Island', NULL, NULL),
(74, 'E', 'E11', 'Maduvvari', '??????????', 'Inhabited Island', NULL, NULL),
(75, 'E', 'E12', 'Inguraidhoo', '???????????', 'Inhabited Island', NULL, NULL),
(76, 'E', 'E13', 'Fainu', '??????', 'Inhabited Island', NULL, NULL),
(77, 'E', 'E14', 'Meedhoo', '????', 'Inhabited Island', NULL, NULL),
(78, 'E', 'E15', 'Kinolhas', '????????', 'Inhabited Island', NULL, NULL),
(79, 'E', 'E05', 'Hulhudhuffaaru', '????????????', 'Inhabited Island', NULL, NULL),
(80, 'E', '', 'Meedhupparu', '??????????', 'Resort Island', NULL, NULL),
(81, 'E', 'E', 'Dhuvaafaru', '????????', 'Inhabited Island', NULL, NULL),
(82, 'F', 'F01', 'Kudarikilu', '??????????', 'Inhabited Island', NULL, NULL),
(83, 'F', 'F02', 'Kamadhoo', '??????', 'Inhabited Island', NULL, NULL),
(84, 'F', 'F03', 'Kendhoo', '??????', 'Inhabited Island', NULL, NULL),
(85, 'F', 'F04', 'Kihaadhoo', '??????', 'Inhabited Island', NULL, NULL),
(86, 'F', 'F05', 'Dhonfanu', '????????', 'Inhabited Island', NULL, NULL),
(87, 'F', 'F06', 'Dharavandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(88, 'F', 'F07', 'Maalhos', '??????', 'Inhabited Island', NULL, NULL),
(89, 'F', 'F08', 'Eydhafushi', '????????', 'Inhabited Island', NULL, NULL),
(90, 'F', 'F09', 'Thulhaadhoo', '??????', 'Inhabited Island', NULL, NULL),
(91, 'F', 'F10', 'Hithaadhoo', '??????', 'Inhabited Island', NULL, NULL),
(92, 'F', 'F11', 'Fulhadhoo', '??????', 'Inhabited Island', NULL, NULL),
(93, 'F', 'F12', 'Fehendhoo', '????????', 'Inhabited Island', NULL, NULL),
(94, 'F', 'F13', 'Goidhoo', '??????', 'Inhabited Island', NULL, NULL),
(95, 'F', '', 'Landaagiraavaru', '?????????????', 'Resort Island', NULL, NULL),
(96, 'F', '', 'Dhunikolhu', '????????', 'Resort Island', NULL, NULL),
(97, 'F', '', 'Kihaadhuffaru', '????????????', 'Resort Island', NULL, NULL),
(98, 'F', '', 'Fonimagoodhoo', '??????????', 'Resort Island', NULL, NULL),
(99, 'F', '', 'Kunfunadhoo', '??????????', 'Resort Island', NULL, NULL),
(100, 'F', '', 'Horubadhoo', '?????????', 'Resort Island', NULL, NULL),
(101, 'F', '', 'Kihavah Haruvalhi', '??????????????????', 'Resort Island', NULL, NULL),
(102, 'F', '', 'Mudhdhoo', '??????', 'Resort Island', NULL, NULL),
(103, 'G', 'G01', 'Hinnavaru', '??????????', 'Inhabited Island', NULL, NULL),
(104, 'G', 'G02', 'Naifaru', '????????', 'Inhabited Island', NULL, NULL),
(105, 'G', 'G03', 'Kurendhoo', '????????', 'Inhabited Island', NULL, NULL),
(106, 'G', 'G04', 'Olhuvelifushi', '????????????', 'Inhabited Island', NULL, NULL),
(107, 'G', 'G05', 'Maafilaafushi', '??????????', 'Inhabited Island', NULL, NULL),
(108, 'G', 'G', 'Felivaru', '????????', 'Inhabited Island', NULL, NULL),
(109, 'G', '', 'Kanuhuraa', '????????', 'Resort Island', NULL, NULL),
(110, 'G', '', 'Komandoo', '????????', 'Resort Island', NULL, NULL),
(111, 'G', '', 'Kuredhdhoo', '????????', 'Resort Island', NULL, NULL),
(112, 'G', '', 'Madhiriguraidhoo', '??????????????', 'Resort Island', NULL, NULL),
(113, 'T', 'T10', 'Male''', '????', 'Inhabited Island', NULL, NULL),
(114, 'T', '', 'Villingili', '?????????', 'Inhabited Island', NULL, NULL),
(115, 'T', '', 'Hulhumale''', '????????', 'Inhabited Island', NULL, NULL),
(116, 'H', 'H01', 'Kaashidhoo', '??????', 'Inhabited Island', NULL, NULL),
(117, 'H', 'H02', 'Gaafaru', '??????', 'Inhabited Island', NULL, NULL),
(118, 'H', 'H03', 'Dhiffushi', '????????', 'Inhabited Island', NULL, NULL),
(119, 'H', 'H04', 'Thulusdhoo', '????????', 'Inhabited Island', NULL, NULL),
(120, 'H', 'H05', 'Huraa', '????', 'Inhabited Island', NULL, NULL),
(121, 'H', 'H06', 'Himmafushi', '??????????', 'Inhabited Island', NULL, NULL),
(122, 'H', 'H07', 'Gulhi', '????', 'Inhabited Island', NULL, NULL),
(123, 'H', 'H09', 'Maafushi', '??????', 'Inhabited Island', NULL, NULL),
(124, 'H', 'H08', 'Guraidhoo', '????????', 'Inhabited Island', NULL, NULL),
(125, 'H', '', 'Asdhu', '??????', 'Resort Island', NULL, NULL),
(126, 'H', '', 'Bandos', '???????', 'Resort Island', NULL, NULL),
(127, 'H', '', 'Baros', '??????', 'Resort Island', NULL, NULL),
(128, 'H', '', 'Biyaadhu', '??????', 'Resort Island', NULL, NULL),
(129, 'H', '', 'Boduhithi', '????????', 'Resort Island', NULL, NULL),
(130, 'H', '', 'Bolifushi', '????????', 'Resort Island', NULL, NULL),
(131, 'H', '', 'Farukolhufushi', '????????????', 'Resort Island', NULL, NULL),
(132, 'H', '', 'Rannalhi', '????????', 'Resort Island', NULL, NULL),
(133, 'H', '', 'Makunufushi', '??????????', 'Resort Island', NULL, NULL),
(134, 'H', '', 'Dhigufinohlu', '??????????', 'Resort Island', NULL, NULL),
(135, 'H', '', 'Emboodhu', '???????', 'Resort Island', NULL, NULL),
(136, 'H', '', 'Eriyadhoo', '????????', 'Resort Island', NULL, NULL),
(137, 'H', '', 'Fihaalhohi', '????????', 'Resort Island', NULL, NULL),
(138, 'H', '', 'Kuda Huraa', '????????', 'Resort Island', NULL, NULL),
(139, 'H', '', 'Furanafushi', '??????????', 'Resort Island', NULL, NULL),
(140, 'H', '', 'Bodufinolhu', '??????????', 'Resort Island', NULL, NULL),
(141, 'H', '', 'Gasfinolhu', '??????????', 'Resort Island', NULL, NULL),
(142, 'H', '', 'Giraavaru', '????????', 'Resort Island', NULL, NULL),
(143, 'H', '', 'Helegeli', '?????????', 'Resort Island', NULL, NULL),
(144, 'H', '', 'Lankanfushi', '????????????', 'Resort Island', NULL, NULL),
(145, 'H', '', 'Ihuru', '??????', 'Resort Island', NULL, NULL),
(146, 'H', '', 'Kandoomafushi', '???????????', 'Resort Island', NULL, NULL),
(147, 'H', '', 'Kanifinolhu', '??????????', 'Resort Island', NULL, NULL),
(148, 'H', '', 'Kudahithi', '????????', 'Resort Island', NULL, NULL),
(149, 'H', '', 'Vihamanaafushi', '????????????', 'Resort Island', NULL, NULL),
(150, 'H', '', 'Velassaru', '????????', 'Resort Island', NULL, NULL),
(151, 'H', '', 'Lhohifushi', '????????', 'Resort Island', NULL, NULL),
(152, 'H', '', 'Mankunudhoo', '????????', 'Resort Island', NULL, NULL),
(153, 'H', '', 'Meerufenfushi', '????????????', 'Resort Island', NULL, NULL),
(154, 'H', '', 'Nakatchafushi', '????????????', 'Resort Island', NULL, NULL),
(155, 'H', '', 'Olhuveli', '????????', 'Resort Island', NULL, NULL),
(156, 'H', '', 'Veligandu Huraa', '?????????????', 'Resort Island', NULL, NULL),
(157, 'H', '', 'Lankanfinolhu', '??????????????', 'Resort Island', NULL, NULL),
(158, 'H', '', 'Medhufinolhu', '??????????', 'Resort Island', NULL, NULL),
(159, 'H', '', 'Mahaanaelhi Huraa', '??????????????', 'Resort Island', NULL, NULL),
(160, 'H', '', 'Ziyaaraiyfushi', '????????????', 'Resort Island', NULL, NULL),
(161, 'H', '', 'Kanuoiy Huraa', '????????????', 'Resort Island', NULL, NULL),
(162, 'H', '', 'Thulhagiri', '????????', 'Resort Island', NULL, NULL),
(163, 'H', '', 'Embudhu Finolhu', '??????? ??????', 'Resort Island', NULL, NULL),
(164, 'H', '', 'Vaadhu', '????', 'Resort Island', NULL, NULL),
(165, 'H', '', 'Vabbinfaru', '????????????', 'Resort Island', NULL, NULL),
(166, 'H', '', 'Vilivaru', '????????', 'Resort Island', NULL, NULL),
(167, 'H', '', 'Hembadhoo', '???????', 'Resort Island', NULL, NULL),
(168, 'U', 'U01', 'Thoddoo', '??????', 'Inhabited Island', NULL, NULL),
(169, 'U', 'U02', 'Rasdhoo', '??????', 'Inhabited Island', NULL, NULL),
(170, 'U', 'U03', 'Ukulhas', '????????', 'Inhabited Island', NULL, NULL),
(171, 'U', 'U04', 'Mathiveri', '????????', 'Inhabited Island', NULL, NULL),
(172, 'U', 'U05', 'Bodufolhudhoo', '??????????', 'Inhabited Island', NULL, NULL),
(173, 'U', 'U06', 'Feridhoo', '??????', 'Inhabited Island', NULL, NULL),
(174, 'U', 'U07', 'Maalhos', '??????', 'Inhabited Island', NULL, NULL),
(175, 'U', 'U08', 'Himandhoo', '????????', 'Inhabited Island', NULL, NULL),
(176, 'U', '', 'Bathala', '??????', 'Resort Island', NULL, NULL),
(177, 'U', '', 'Ellaidhu', '??????????', 'Resort Island', NULL, NULL),
(178, 'U', '', 'Fesdhoo', '??????', 'Resort Island', NULL, NULL),
(179, 'U', '', 'Gangehi', '????????', 'Resort Island', NULL, NULL),
(180, 'U', '', 'Halaveli', '????????', 'Resort Island', NULL, NULL),
(181, 'U', '', 'Kuramathi', '????????', 'Resort Island', NULL, NULL),
(182, 'U', '', 'Maayaafushi', '????????', 'Resort Island', NULL, NULL),
(183, 'U', '', 'Madoogali', '????????', 'Resort Island', NULL, NULL),
(184, 'U', '', 'Kudafolhudhoo', '??????????', 'Resort Island', NULL, NULL),
(185, 'U', '', 'Veligandu', '?????????', 'Resort Island', NULL, NULL),
(186, 'U', '', 'Velidhoo', '??????', 'Resort Island', NULL, NULL),
(187, 'I', 'I01', 'Hangnameedhoo', '??????????', 'Inhabited Island', NULL, NULL),
(188, 'I', 'I02', 'Omadhoo', '??????', 'Inhabited Island', NULL, NULL),
(189, 'I', 'I03', 'Kuburudhoo', '?????????', 'Inhabited Island', NULL, NULL),
(190, 'I', 'I04', 'Mahibadhoo', '????????', 'Inhabited Island', NULL, NULL),
(191, 'I', 'I05', 'Mandhoo', '??????', 'Inhabited Island', NULL, NULL),
(192, 'I', 'I06', 'Dhangethi', '???????', 'Inhabited Island', NULL, NULL),
(193, 'I', 'I07', 'Dhigurah', '????????', 'Inhabited Island', NULL, NULL),
(194, 'I', 'I08', 'Fenfushi', '????????', 'Inhabited Island', NULL, NULL),
(195, 'I', 'I09', 'Dhidhdhoo', '??????', 'Inhabited Island', NULL, NULL),
(196, 'I', 'I10', 'Maamigili', '????????', 'Inhabited Island', NULL, NULL),
(197, 'I', '', 'Angaaga', '???????', 'Resort Island', NULL, NULL),
(198, 'I', '', 'Dhidhdhoofinolhu', '????????????', 'Resort Island', NULL, NULL),
(199, 'I', '', 'Athurugau', '??????????', 'Resort Island', NULL, NULL),
(200, 'I', '', 'Dhiffushi', '????????', 'Resort Island', NULL, NULL),
(201, 'I', '', 'Kuda Rah', '????????', 'Resort Island', NULL, NULL),
(202, 'I', '', 'Huvahendhoo', '??????????', 'Resort Island', NULL, NULL),
(203, 'I', '', 'Machchafushi', '??????????', 'Resort Island', NULL, NULL),
(204, 'I', '', 'Mirihi', '??????', 'Resort Island', NULL, NULL),
(205, 'I', '', 'Rangalifinolhu', '??????????????', 'Resort Island', NULL, NULL),
(206, 'I', '', 'Moofushi', '??????', 'Resort Island', NULL, NULL),
(207, 'I', '', 'Villingilivaru', '?????????????', 'Resort Island', NULL, NULL),
(208, 'I', '', 'Nalaguraidhoo', '????????????', 'Resort Island', NULL, NULL),
(209, 'I', '', 'Thundufushi', '?????????', 'Resort Island', NULL, NULL),
(210, 'I', '', 'Maafushivaru', '??????????', 'Resort Island', NULL, NULL),
(211, 'I', '', 'Vakarufalhi', '??????????', 'Resort Island', NULL, NULL),
(212, 'I', '', 'Vilamendhoo', '??????????', 'Resort Island', NULL, NULL),
(213, 'J', 'J01', 'Fulidhoo', '??????', 'Inhabited Island', NULL, NULL),
(214, 'J', 'J02', 'Thinadhoo', '??????', 'Inhabited Island', NULL, NULL),
(215, 'J', 'J03', 'Felidhoo', '??????', 'Inhabited Island', NULL, NULL),
(216, 'J', 'J04', 'Keyodhoo', '??????', 'Inhabited Island', NULL, NULL),
(217, 'J', 'J05', 'Rakeedhoo', '??????', 'Inhabited Island', NULL, NULL),
(218, 'J', '', 'Alimatha', '????????', 'Resort Island', NULL, NULL),
(219, 'J', '', 'Dhiggiri', '????????', 'Resort Island', NULL, NULL),
(220, 'K', 'K01', 'Raimandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(221, 'K', 'K03', 'Veyvah', '??????', 'Inhabited Island', NULL, NULL),
(222, 'K', 'K04', 'Mulah', '??????', 'Inhabited Island', NULL, NULL),
(223, 'K', 'K05', 'Muli', '????', 'Inhabited Island', NULL, NULL),
(224, 'K', 'K06', 'Naalaafushi', '????????', 'Inhabited Island', NULL, NULL),
(225, 'K', 'K07', 'Kolhufushi', '????????', 'Inhabited Island', NULL, NULL),
(226, 'K', 'K08', 'Dhiggaru', '????????', 'Inhabited Island', NULL, NULL),
(227, 'K', 'K09', 'Maduvvari', '??????????', 'Inhabited Island', NULL, NULL),
(228, 'K', '', 'Hakuraa Huraa', '??????????', 'Resort Island', NULL, NULL),
(229, 'K', '', 'Medhufushi', '????????', 'Resort Island', NULL, NULL),
(230, 'L', 'L01', 'Feeali', '??????', 'Inhabited Island', NULL, NULL),
(231, 'L', 'L02', 'Biledhdhoo', '????????', 'Inhabited Island', NULL, NULL),
(232, 'L', 'L03', 'Magoodhoo', '??????', 'Inhabited Island', NULL, NULL),
(233, 'L', 'L04', 'Dharaboodhoo', '?????????', 'Inhabited Island', NULL, NULL),
(234, 'L', 'L05', 'Nilandhoo', '????????', 'Inhabited Island', NULL, NULL),
(235, 'L', '', 'Filitheyo', '????????', 'Resort Island', NULL, NULL),
(236, 'M', 'M01', 'Meedhoo', '????', 'Inhabited Island', NULL, NULL),
(237, 'M', 'M02', 'Bandidhoo', '???????', 'Inhabited Island', NULL, NULL),
(238, 'M', 'M03', 'Rinbudhoo', '???????', 'Inhabited Island', NULL, NULL),
(239, 'M', 'M04', 'Hulhudheli', '????????', 'Inhabited Island', NULL, NULL),
(240, 'M', 'M05', 'Gemendhoo', '????????', 'Inhabited Island', NULL, NULL),
(241, 'M', 'M06', 'Vaanee', '????', 'Inhabited Island', NULL, NULL),
(242, 'M', 'M07', 'Maaenboodhoo', '?????????', 'Inhabited Island', NULL, NULL),
(243, 'M', 'M08', 'Kudahuvadhoo', '??????????', 'Inhabited Island', NULL, NULL),
(244, 'M', '', 'Velavaru', '????????', 'Resort Island', NULL, NULL),
(245, 'M', '', 'Meedhuffushi', '??????????', 'Resort Island', NULL, NULL),
(246, 'M', '', 'Olhuveli', '????????', 'Resort Island', NULL, NULL),
(247, 'N', 'N01', 'Buruni', '??????', 'Inhabited Island', NULL, NULL),
(248, 'N', 'N02', 'Vilufushi', '????????', 'Inhabited Island', NULL, NULL),
(249, 'N', 'N03', 'Madifushi', '????????', 'Inhabited Island', NULL, NULL),
(250, 'N', 'N04', 'Dhiyamigili', '??????????', 'Inhabited Island', NULL, NULL),
(251, 'N', 'N05', 'Guraidhoo', '????????', 'Inhabited Island', NULL, NULL),
(252, 'N', 'N06', 'Kandoodhoo', '???????', 'Inhabited Island', NULL, NULL),
(253, 'N', 'N07', 'Vandhoo', '??????', 'Inhabited Island', NULL, NULL),
(254, 'N', 'N08', 'Hirilandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(255, 'N', 'N09', 'Gaadhiffushi', '??????????', 'Inhabited Island', NULL, NULL),
(256, 'N', 'N10', 'Thimarafushi', '??????????', 'Inhabited Island', NULL, NULL),
(257, 'N', 'N11', 'Veymandoo', '????????', 'Inhabited Island', NULL, NULL),
(258, 'N', 'N12', 'Kinbidhoo', '???????', 'Inhabited Island', NULL, NULL),
(259, 'N', 'N13', 'Omadhoo', '??????', 'Inhabited Island', NULL, NULL),
(260, 'O', 'O01', 'Isdhoo', '??????', 'Inhabited Island', NULL, NULL),
(261, 'O', 'O02', 'Dhabidhoo', '???????', 'Inhabited Island', NULL, NULL),
(262, 'O', 'O03', 'Maabaidhoo', '????????', 'Inhabited Island', NULL, NULL),
(263, 'O', 'O04', 'Mundoo', '??????', 'Inhabited Island', NULL, NULL),
(264, 'O', 'O05', 'Kalhaidhoo', '????????', 'Inhabited Island', NULL, NULL),
(265, 'O', 'O06', 'Gan', '????', 'Inhabited Island', NULL, NULL),
(266, 'O', 'O07', 'Maavah', '??????', 'Inhabited Island', NULL, NULL),
(267, 'O', 'O08', 'Fonadhoo', '??????', 'Inhabited Island', NULL, NULL),
(268, 'O', 'O09', 'Gaadhoo', '????', 'Inhabited Island', NULL, NULL),
(269, 'O', 'O10', 'Maamendhoo', '????????', 'Inhabited Island', NULL, NULL),
(270, 'O', 'O11', 'Hithadhoo', '??????', 'Inhabited Island', NULL, NULL),
(271, 'O', 'O12', 'Kunahandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(272, 'O', '', 'Olhuveli', '????????', 'Resort Island', NULL, NULL),
(273, 'P', 'P01', 'Kolamaafushi', '??????????', 'Inhabited Island', NULL, NULL),
(274, 'P', 'P02', 'Villingili', '?????????', 'Inhabited Island', NULL, NULL),
(275, 'P', 'P03', 'Maamendhoo', '????????', 'Inhabited Island', NULL, NULL),
(276, 'P', 'P04', 'Nilandhoo', '????????', 'Inhabited Island', NULL, NULL),
(277, 'P', 'P05', 'Dhaandhoo', '??????', 'Inhabited Island', NULL, NULL),
(278, 'P', 'P06', 'Dhevvadhoo', '????????', 'Inhabited Island', NULL, NULL),
(279, 'P', 'P07', 'Kondey', '?????', 'Inhabited Island', NULL, NULL),
(280, 'P', 'P08', 'Dhiyadhoo', '??????', 'Inhabited Island', NULL, NULL),
(281, 'P', 'P09', 'Gemanafushi', '??????????', 'Inhabited Island', NULL, NULL),
(282, 'P', 'P10', 'Kanduhulhudhoo', '???????????', 'Inhabited Island', NULL, NULL),
(283, 'P', '', 'Falhumaafushi', '??????????', 'Resort Island', NULL, NULL),
(284, 'P', '', 'Funamauddoo', '????????????', 'Resort Island', NULL, NULL),
(285, 'P', '', 'Hadahaa', '??????', 'Resort Island', NULL, NULL),
(286, 'P', '', 'Meradhoo', '??????', 'Resort Island', NULL, NULL),
(287, 'Q', 'Q01', 'Madaveli', '????????', 'Inhabited Island', NULL, NULL),
(288, 'Q', 'Q02', 'Hoadedhdhoo', '????????', 'Inhabited Island', NULL, NULL),
(289, 'Q', 'Q03', 'Nadellaa', '????????', 'Inhabited Island', NULL, NULL),
(290, 'Q', 'Q04', 'Gadhdhoo', '??????', 'Inhabited Island', NULL, NULL),
(291, 'Q', 'Q05', 'Rathafandhoo', '??????????', 'Inhabited Island', NULL, NULL),
(292, 'Q', 'Q06', 'Vaadhoo', '????', 'Inhabited Island', NULL, NULL),
(293, 'Q', 'Q07', 'Fiyoari', '??????', 'Inhabited Island', NULL, NULL),
(294, 'Q', 'Q10', 'Thinadhoo', '??????', 'Inhabited Island', NULL, NULL),
(295, 'Q', 'Q', 'Fares-Maathodaa', '????????????', 'Inhabited Island', NULL, NULL),
(296, 'Q', '', 'Magudhdhuvaa', '??????????', 'Resort Island', NULL, NULL),
(297, 'R', 'R01', 'Fuvahmulah', '????????????', 'Inhabited Island', NULL, NULL),
(298, 'S', 'S01', 'Meedhoo', '????', 'Inhabited Island', NULL, NULL),
(299, 'S', 'S02', 'Hithadhoo', '??????', 'Inhabited Island', NULL, NULL),
(300, 'S', 'S03', 'Maradhoo', '??????', 'Inhabited Island', NULL, NULL),
(301, 'S', 'S04', 'Feydhoo', '????', 'Inhabited Island', NULL, NULL),
(302, 'S', 'S05', 'Maradhoo-Feydhoo', '??????????', 'Inhabited Island', NULL, NULL),
(303, 'S', 'S06', 'Hulhudhoo', '??????', 'Inhabited Island', NULL, NULL),
(304, 'S', 'S', 'Hulhumeedhoo', '????????', 'Inhabited Island', NULL, NULL),
(305, 'S', '', 'Herethere', '????????', 'Resort Island', NULL, NULL),
(306, 'S', '', 'Villingili', '?????????', 'Resort Island', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2017_05_03_181449_create_atolls_table', 1),
(3, '2017_05_03_194603_create_islands_table', 1),
(4, '2017_05_04_112108_create_city_councils_table', 1),
(5, '2017_05_04_113529_create_atoll_councils_table', 1),
(6, '2017_05_04_130808_create_atoll_council_sections_table', 1),
(7, '2017_05_04_131920_create_atoll_council_section_ballot_boxes_table', 1),
(8, '2017_05_04_132248_create_atoll_council_section_ballot_box_details_table', 1),
(9, '2017_05_04_153158_create_atoll_council_section_candidates_table', 2),
(10, '2017_05_04_153252_create_political_parties_table', 2),
(11, '2017_05_04_153728_create_candidates_table', 2),
(12, '2017_05_04_164648_create_ballot_boxes_table', 3),
(13, '2017_05_04_164828_create_ballot_box_details_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `political_parties`
--

CREATE TABLE `political_parties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `political_parties`
--

INSERT INTO `political_parties` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'MDP', NULL, NULL),
(2, 'PPM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atolls`
--
ALTER TABLE `atolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_councils`
--
ALTER TABLE `atoll_councils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_sections`
--
ALTER TABLE `atoll_council_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_ballot_boxes`
--
ALTER TABLE `atoll_council_section_ballot_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_ballot_box_details`
--
ALTER TABLE `atoll_council_section_ballot_box_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_candidates`
--
ALTER TABLE `atoll_council_section_candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ballot_boxes`
--
ALTER TABLE `ballot_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ballot_box_details`
--
ALTER TABLE `ballot_box_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_councils`
--
ALTER TABLE `city_councils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `islands`
--
ALTER TABLE `islands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `political_parties`
--
ALTER TABLE `political_parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atolls`
--
ALTER TABLE `atolls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `atoll_councils`
--
ALTER TABLE `atoll_councils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `atoll_council_sections`
--
ALTER TABLE `atoll_council_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `atoll_council_section_ballot_boxes`
--
ALTER TABLE `atoll_council_section_ballot_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atoll_council_section_ballot_box_details`
--
ALTER TABLE `atoll_council_section_ballot_box_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atoll_council_section_candidates`
--
ALTER TABLE `atoll_council_section_candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ballot_boxes`
--
ALTER TABLE `ballot_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ballot_box_details`
--
ALTER TABLE `ballot_box_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `city_councils`
--
ALTER TABLE `city_councils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `islands`
--
ALTER TABLE `islands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `political_parties`
--
ALTER TABLE `political_parties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
