-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2017 at 06:51 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `councilelections`
--

-- --------------------------------------------------------

--
-- Table structure for table `atolls`
--

CREATE TABLE `atolls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_atoll_name_long` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_atoll_name_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_official_atoll_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_atoll_name_long` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_atoll_name_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_official_atoll_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atolls`
--

INSERT INTO `atolls` (`id`, `name`, `reg_no`, `english_atoll_name_long`, `english_atoll_name_short`, `english_official_atoll_name`, `dhivehi_atoll_name_long`, `dhivehi_atoll_name_short`, `dhivehi_official_atoll_name`, `created_at`, `updated_at`) VALUES
(1, 'A', 'A', 'Haa Alif', 'HA', 'Thiladhunmathi Uthuruburi', '?? ??????', '??', '???????????? ??????????', NULL, NULL),
(2, 'B', 'B', 'Haa Dhaalu', 'HDh', 'Thiladhunmathi Dhekunuburi', '?? ????', '??', '???????????? ??????????', NULL, NULL),
(3, 'C', 'C', 'Shaviyani', 'Sh', 'Miladhunmadulu Uthuruburi', '????????', '?', '?????????????? ??????????', NULL, NULL),
(4, 'D', 'D', 'Noonu', 'N', 'Miladhunmadulu Dhekunuburi', '????', '?', '?????????????? ??????????', NULL, NULL),
(5, 'E', 'E', 'Raa', 'R', 'Maalhosmadulu Uthuruburi', '??', '?', '???????????? ??????????', NULL, NULL),
(6, 'F', 'F', 'Baa', 'B', 'Maalhosmadulu Dhekunuburi', '??', '?', '???????????? ??????????', NULL, NULL),
(7, 'G', 'G', 'Lhaviyani', 'Lh', 'Faadhippolhu', '????????', '?', '??????????', NULL, NULL),
(8, 'H', 'H', 'Kaafu', 'K', 'Mal? Atholhu', '????', '?', '???? ??????', NULL, NULL),
(9, 'U', 'U', 'Alif Alif', 'AA', 'Ari Atholhu Uthuruburi', '?????? ??????', '??', '?????????? ??????????', NULL, NULL),
(10, 'I', 'I', 'Alif Dhaal', 'Adh', 'Ari Atholhu Dhekunuburi', '?????? ????', '??', '?????????? ??????????', NULL, NULL),
(11, 'J', 'J', 'Vaavu', 'V', 'Felidhu Atholhu', '????', '?', '?????? ??????', NULL, NULL),
(12, 'K', 'K', 'Meemu', 'M', 'Mulak Atholhu', '????', '?', '?????? ??????', NULL, NULL),
(13, 'L', 'L', 'Faafu', 'F', 'Nilandhe Atholhu Uthuruburi', '????', '?', '???????? ?????? ??????????', NULL, NULL),
(14, 'M', 'M', 'Dhaalu', 'Dh', 'Nilandhe Atholhu Dhekunuburi', '????', '?', '???????? ?????? ??????????', NULL, NULL),
(15, 'N', 'N', 'Thaa', 'Th', 'Kolhumadulu', '??', '?', '??????????', NULL, NULL),
(16, 'O', 'O', 'Laamu', 'L', 'Haddhunmathi', '????', '?', '????????????', NULL, NULL),
(17, 'P', 'P', 'Gaafu Alif', 'GA', 'Huvadhu Atholhu Uthuruburi', '???? ??????', '??', '?????? ?????? ??????????', NULL, NULL),
(18, 'Q', 'Q', 'Gaafu Dhaalu', 'GDh', 'Huvadhu Atholhu Dhekunuburi', '???? ????', '??', '?????? ?????? ??????????', NULL, NULL),
(19, 'R', 'R', 'Gnaviyani', 'Gn', 'Fuvahmulah', '????????', '?', '????????????', NULL, NULL),
(20, 'S', 'S', 'Seenu', 'S', 'Addu City', '????', '?', '?????? ????', NULL, NULL),
(21, 'T', 'T', 'Male''', 'Male''', 'Male'' City', '????', '', '???? ????', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `atoll_councils`
--

CREATE TABLE `atoll_councils` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_councils`
--

INSERT INTO `atoll_councils` (`id`, `atoll_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(6, 6, NULL, NULL),
(7, 7, NULL, NULL),
(8, 8, NULL, NULL),
(9, 9, NULL, NULL),
(10, 10, NULL, NULL),
(11, 11, NULL, NULL),
(12, 12, NULL, NULL),
(13, 13, NULL, NULL),
(14, 14, NULL, NULL),
(15, 15, NULL, NULL),
(16, 16, NULL, NULL),
(17, 17, NULL, NULL),
(18, 18, NULL, NULL),
(19, 19, NULL, NULL),
(20, 20, NULL, NULL),
(21, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_sections`
--

CREATE TABLE `atoll_council_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_council_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_council_sections`
--

INSERT INTO `atoll_council_sections` (`id`, `atoll_council_id`, `name`, `created_at`, `updated_at`) VALUES
(4, 1, 'Ihavandhoo', '2017-05-04 11:08:51', '2017-05-04 11:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_ballot_boxes`
--

CREATE TABLE `atoll_council_section_ballot_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_ballot_box_details`
--

CREATE TABLE `atoll_council_section_ballot_box_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_section_ballot_box_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `total_votes` int(11) NOT NULL,
  `valid_votes` int(11) NOT NULL,
  `invalid_votes` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atoll_council_section_candidates`
--

CREATE TABLE `atoll_council_section_candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_council_section_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atoll_council_section_candidates`
--

INSERT INTO `atoll_council_section_candidates` (`id`, `atoll_council_section_id`, `candidate_id`, `created_at`, `updated_at`) VALUES
(3, 4, 6, '2017-05-04 11:33:10', '2017-05-04 11:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `political_party_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `name`, `number`, `political_party_id`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed', '02', 1, '2017-05-04 10:53:55', '2017-05-04 10:53:55'),
(2, 'Shimad Firaaq', '03', 1, '2017-05-04 10:54:29', '2017-05-04 10:54:29'),
(3, 'Shimad Firaaq', '03', 1, '2017-05-04 10:54:49', '2017-05-04 10:54:49'),
(4, 'Shimad Firaaq', '03', 1, '2017-05-04 10:55:11', '2017-05-04 10:55:11'),
(5, 'Mohammed Aboobakuru', '03', 2, '2017-05-04 10:57:23', '2017-05-04 10:57:23'),
(6, 'Shimad MOhammed', '03', 1, '2017-05-04 11:33:10', '2017-05-04 11:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `city_councils`
--

CREATE TABLE `city_councils` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `islands`
--

CREATE TABLE `islands` (
  `id` int(10) UNSIGNED NOT NULL,
  `atoll_reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `island_reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_official_island_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dhivehi_official_island_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `island_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `islands`
--

INSERT INTO `islands` (`id`, `atoll_reg_no`, `island_reg_no`, `english_official_island_name`, `dhivehi_official_island_name`, `island_type`, `created_at`, `updated_at`) VALUES
(1, 'fwaf', 'fawfawf', 'fawfawf', 'fawfawf', 'fawfawf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2017_05_03_181449_create_atolls_table', 1),
(3, '2017_05_03_194603_create_islands_table', 1),
(4, '2017_05_04_112108_create_city_councils_table', 1),
(5, '2017_05_04_113529_create_atoll_councils_table', 1),
(6, '2017_05_04_130808_create_atoll_council_sections_table', 1),
(7, '2017_05_04_131920_create_atoll_council_section_ballot_boxes_table', 1),
(8, '2017_05_04_132248_create_atoll_council_section_ballot_box_details_table', 1),
(9, '2017_05_04_153158_create_atoll_council_section_candidates_table', 2),
(10, '2017_05_04_153252_create_political_parties_table', 2),
(11, '2017_05_04_153728_create_candidates_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `political_parties`
--

CREATE TABLE `political_parties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `political_parties`
--

INSERT INTO `political_parties` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'MDP', NULL, NULL),
(2, 'PPM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atolls`
--
ALTER TABLE `atolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_councils`
--
ALTER TABLE `atoll_councils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_sections`
--
ALTER TABLE `atoll_council_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_ballot_boxes`
--
ALTER TABLE `atoll_council_section_ballot_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_ballot_box_details`
--
ALTER TABLE `atoll_council_section_ballot_box_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atoll_council_section_candidates`
--
ALTER TABLE `atoll_council_section_candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_councils`
--
ALTER TABLE `city_councils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `islands`
--
ALTER TABLE `islands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `political_parties`
--
ALTER TABLE `political_parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atolls`
--
ALTER TABLE `atolls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `atoll_councils`
--
ALTER TABLE `atoll_councils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `atoll_council_sections`
--
ALTER TABLE `atoll_council_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `atoll_council_section_ballot_boxes`
--
ALTER TABLE `atoll_council_section_ballot_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atoll_council_section_ballot_box_details`
--
ALTER TABLE `atoll_council_section_ballot_box_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atoll_council_section_candidates`
--
ALTER TABLE `atoll_council_section_candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `city_councils`
--
ALTER TABLE `city_councils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `islands`
--
ALTER TABLE `islands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `political_parties`
--
ALTER TABLE `political_parties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
